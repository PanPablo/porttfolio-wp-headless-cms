-- MySQL dump 10.13  Distrib 5.7.22, for osx10.13 (x86_64)
--
-- Host: 127.0.0.1    Database: react
-- ------------------------------------------------------
-- Server version	5.7.22

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `ra_commentmeta`
--

DROP TABLE IF EXISTS `ra_commentmeta`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `ra_commentmeta` (
  `meta_id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `comment_id` bigint(20) unsigned NOT NULL DEFAULT '0',
  `meta_key` varchar(255) COLLATE utf8mb4_unicode_520_ci DEFAULT NULL,
  `meta_value` longtext COLLATE utf8mb4_unicode_520_ci,
  PRIMARY KEY (`meta_id`),
  KEY `comment_id` (`comment_id`),
  KEY `meta_key` (`meta_key`(191))
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_520_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `ra_commentmeta`
--

LOCK TABLES `ra_commentmeta` WRITE;
/*!40000 ALTER TABLE `ra_commentmeta` DISABLE KEYS */;
/*!40000 ALTER TABLE `ra_commentmeta` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `ra_comments`
--

DROP TABLE IF EXISTS `ra_comments`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `ra_comments` (
  `comment_ID` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `comment_post_ID` bigint(20) unsigned NOT NULL DEFAULT '0',
  `comment_author` tinytext COLLATE utf8mb4_unicode_520_ci NOT NULL,
  `comment_author_email` varchar(100) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `comment_author_url` varchar(200) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `comment_author_IP` varchar(100) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `comment_date` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `comment_date_gmt` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `comment_content` text COLLATE utf8mb4_unicode_520_ci NOT NULL,
  `comment_karma` int(11) NOT NULL DEFAULT '0',
  `comment_approved` varchar(20) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '1',
  `comment_agent` varchar(255) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `comment_type` varchar(20) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `comment_parent` bigint(20) unsigned NOT NULL DEFAULT '0',
  `user_id` bigint(20) unsigned NOT NULL DEFAULT '0',
  PRIMARY KEY (`comment_ID`),
  KEY `comment_post_ID` (`comment_post_ID`),
  KEY `comment_approved_date_gmt` (`comment_approved`,`comment_date_gmt`),
  KEY `comment_date_gmt` (`comment_date_gmt`),
  KEY `comment_parent` (`comment_parent`),
  KEY `comment_author_email` (`comment_author_email`(10))
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_520_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `ra_comments`
--

LOCK TABLES `ra_comments` WRITE;
/*!40000 ALTER TABLE `ra_comments` DISABLE KEYS */;
INSERT INTO `ra_comments` VALUES (1,1,'Komentator WordPress','wapuu@wordpress.example','https://wordpress.org/','','2019-03-24 23:55:13','2019-03-24 22:55:13','Cześć, to jest komentarz.\nAby zapoznać się z moderowaniem, edycją i usuwaniem komentarzy, należy odwiedzić ekran Komentarze w kokpicie.\nAwatary komentujących pochodzą z <a href=\"https://pl.gravatar.com\">Gravatara</a>.',0,'1','','',0,0);
/*!40000 ALTER TABLE `ra_comments` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `ra_links`
--

DROP TABLE IF EXISTS `ra_links`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `ra_links` (
  `link_id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `link_url` varchar(255) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `link_name` varchar(255) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `link_image` varchar(255) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `link_target` varchar(25) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `link_description` varchar(255) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `link_visible` varchar(20) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT 'Y',
  `link_owner` bigint(20) unsigned NOT NULL DEFAULT '1',
  `link_rating` int(11) NOT NULL DEFAULT '0',
  `link_updated` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `link_rel` varchar(255) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `link_notes` mediumtext COLLATE utf8mb4_unicode_520_ci NOT NULL,
  `link_rss` varchar(255) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  PRIMARY KEY (`link_id`),
  KEY `link_visible` (`link_visible`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_520_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `ra_links`
--

LOCK TABLES `ra_links` WRITE;
/*!40000 ALTER TABLE `ra_links` DISABLE KEYS */;
/*!40000 ALTER TABLE `ra_links` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `ra_options`
--

DROP TABLE IF EXISTS `ra_options`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `ra_options` (
  `option_id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `option_name` varchar(191) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `option_value` longtext COLLATE utf8mb4_unicode_520_ci NOT NULL,
  `autoload` varchar(20) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT 'yes',
  PRIMARY KEY (`option_id`),
  UNIQUE KEY `option_name` (`option_name`)
) ENGINE=InnoDB AUTO_INCREMENT=453 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_520_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `ra_options`
--

LOCK TABLES `ra_options` WRITE;
/*!40000 ALTER TABLE `ra_options` DISABLE KEYS */;
INSERT INTO `ra_options` VALUES (1,'siteurl','http://localhost/reactwp','yes'),(2,'home','http://localhost/reactwp','yes'),(3,'blogname','React Pablo','yes'),(4,'blogdescription','Kolejna witryna oparta na WordPressie','yes'),(5,'users_can_register','0','yes'),(6,'admin_email','pstruminski@gmail.com','yes'),(7,'start_of_week','1','yes'),(8,'use_balanceTags','0','yes'),(9,'use_smilies','1','yes'),(10,'require_name_email','1','yes'),(11,'comments_notify','1','yes'),(12,'posts_per_rss','10','yes'),(13,'rss_use_excerpt','0','yes'),(14,'mailserver_url','mail.example.com','yes'),(15,'mailserver_login','login@example.com','yes'),(16,'mailserver_pass','password','yes'),(17,'mailserver_port','110','yes'),(18,'default_category','1','yes'),(19,'default_comment_status','open','yes'),(20,'default_ping_status','open','yes'),(21,'default_pingback_flag','1','yes'),(22,'posts_per_page','10','yes'),(23,'date_format','j F Y','yes'),(24,'time_format','H:i','yes'),(25,'links_updated_date_format','j F Y H:i','yes'),(26,'comment_moderation','0','yes'),(27,'moderation_notify','1','yes'),(28,'permalink_structure','/index.php/%year%/%monthnum%/%day%/%postname%/','yes'),(29,'rewrite_rules','a:74:{s:11:\"^wp-json/?$\";s:22:\"index.php?rest_route=/\";s:14:\"^wp-json/(.*)?\";s:33:\"index.php?rest_route=/$matches[1]\";s:21:\"^index.php/wp-json/?$\";s:22:\"index.php?rest_route=/\";s:24:\"^index.php/wp-json/(.*)?\";s:33:\"index.php?rest_route=/$matches[1]\";s:48:\".*wp-(atom|rdf|rss|rss2|feed|commentsrss2)\\.php$\";s:18:\"index.php?feed=old\";s:20:\".*wp-app\\.php(/.*)?$\";s:19:\"index.php?error=403\";s:18:\".*wp-register.php$\";s:23:\"index.php?register=true\";s:42:\"index.php/feed/(feed|rdf|rss|rss2|atom)/?$\";s:27:\"index.php?&feed=$matches[1]\";s:37:\"index.php/(feed|rdf|rss|rss2|atom)/?$\";s:27:\"index.php?&feed=$matches[1]\";s:18:\"index.php/embed/?$\";s:21:\"index.php?&embed=true\";s:30:\"index.php/page/?([0-9]{1,})/?$\";s:28:\"index.php?&paged=$matches[1]\";s:51:\"index.php/comments/feed/(feed|rdf|rss|rss2|atom)/?$\";s:42:\"index.php?&feed=$matches[1]&withcomments=1\";s:46:\"index.php/comments/(feed|rdf|rss|rss2|atom)/?$\";s:42:\"index.php?&feed=$matches[1]&withcomments=1\";s:27:\"index.php/comments/embed/?$\";s:21:\"index.php?&embed=true\";s:54:\"index.php/search/(.+)/feed/(feed|rdf|rss|rss2|atom)/?$\";s:40:\"index.php?s=$matches[1]&feed=$matches[2]\";s:49:\"index.php/search/(.+)/(feed|rdf|rss|rss2|atom)/?$\";s:40:\"index.php?s=$matches[1]&feed=$matches[2]\";s:30:\"index.php/search/(.+)/embed/?$\";s:34:\"index.php?s=$matches[1]&embed=true\";s:42:\"index.php/search/(.+)/page/?([0-9]{1,})/?$\";s:41:\"index.php?s=$matches[1]&paged=$matches[2]\";s:24:\"index.php/search/(.+)/?$\";s:23:\"index.php?s=$matches[1]\";s:57:\"index.php/author/([^/]+)/feed/(feed|rdf|rss|rss2|atom)/?$\";s:50:\"index.php?author_name=$matches[1]&feed=$matches[2]\";s:52:\"index.php/author/([^/]+)/(feed|rdf|rss|rss2|atom)/?$\";s:50:\"index.php?author_name=$matches[1]&feed=$matches[2]\";s:33:\"index.php/author/([^/]+)/embed/?$\";s:44:\"index.php?author_name=$matches[1]&embed=true\";s:45:\"index.php/author/([^/]+)/page/?([0-9]{1,})/?$\";s:51:\"index.php?author_name=$matches[1]&paged=$matches[2]\";s:27:\"index.php/author/([^/]+)/?$\";s:33:\"index.php?author_name=$matches[1]\";s:79:\"index.php/([0-9]{4})/([0-9]{1,2})/([0-9]{1,2})/feed/(feed|rdf|rss|rss2|atom)/?$\";s:80:\"index.php?year=$matches[1]&monthnum=$matches[2]&day=$matches[3]&feed=$matches[4]\";s:74:\"index.php/([0-9]{4})/([0-9]{1,2})/([0-9]{1,2})/(feed|rdf|rss|rss2|atom)/?$\";s:80:\"index.php?year=$matches[1]&monthnum=$matches[2]&day=$matches[3]&feed=$matches[4]\";s:55:\"index.php/([0-9]{4})/([0-9]{1,2})/([0-9]{1,2})/embed/?$\";s:74:\"index.php?year=$matches[1]&monthnum=$matches[2]&day=$matches[3]&embed=true\";s:67:\"index.php/([0-9]{4})/([0-9]{1,2})/([0-9]{1,2})/page/?([0-9]{1,})/?$\";s:81:\"index.php?year=$matches[1]&monthnum=$matches[2]&day=$matches[3]&paged=$matches[4]\";s:49:\"index.php/([0-9]{4})/([0-9]{1,2})/([0-9]{1,2})/?$\";s:63:\"index.php?year=$matches[1]&monthnum=$matches[2]&day=$matches[3]\";s:66:\"index.php/([0-9]{4})/([0-9]{1,2})/feed/(feed|rdf|rss|rss2|atom)/?$\";s:64:\"index.php?year=$matches[1]&monthnum=$matches[2]&feed=$matches[3]\";s:61:\"index.php/([0-9]{4})/([0-9]{1,2})/(feed|rdf|rss|rss2|atom)/?$\";s:64:\"index.php?year=$matches[1]&monthnum=$matches[2]&feed=$matches[3]\";s:42:\"index.php/([0-9]{4})/([0-9]{1,2})/embed/?$\";s:58:\"index.php?year=$matches[1]&monthnum=$matches[2]&embed=true\";s:54:\"index.php/([0-9]{4})/([0-9]{1,2})/page/?([0-9]{1,})/?$\";s:65:\"index.php?year=$matches[1]&monthnum=$matches[2]&paged=$matches[3]\";s:36:\"index.php/([0-9]{4})/([0-9]{1,2})/?$\";s:47:\"index.php?year=$matches[1]&monthnum=$matches[2]\";s:53:\"index.php/([0-9]{4})/feed/(feed|rdf|rss|rss2|atom)/?$\";s:43:\"index.php?year=$matches[1]&feed=$matches[2]\";s:48:\"index.php/([0-9]{4})/(feed|rdf|rss|rss2|atom)/?$\";s:43:\"index.php?year=$matches[1]&feed=$matches[2]\";s:29:\"index.php/([0-9]{4})/embed/?$\";s:37:\"index.php?year=$matches[1]&embed=true\";s:41:\"index.php/([0-9]{4})/page/?([0-9]{1,})/?$\";s:44:\"index.php?year=$matches[1]&paged=$matches[2]\";s:23:\"index.php/([0-9]{4})/?$\";s:26:\"index.php?year=$matches[1]\";s:68:\"index.php/[0-9]{4}/[0-9]{1,2}/[0-9]{1,2}/[^/]+/attachment/([^/]+)/?$\";s:32:\"index.php?attachment=$matches[1]\";s:78:\"index.php/[0-9]{4}/[0-9]{1,2}/[0-9]{1,2}/[^/]+/attachment/([^/]+)/trackback/?$\";s:37:\"index.php?attachment=$matches[1]&tb=1\";s:98:\"index.php/[0-9]{4}/[0-9]{1,2}/[0-9]{1,2}/[^/]+/attachment/([^/]+)/feed/(feed|rdf|rss|rss2|atom)/?$\";s:49:\"index.php?attachment=$matches[1]&feed=$matches[2]\";s:93:\"index.php/[0-9]{4}/[0-9]{1,2}/[0-9]{1,2}/[^/]+/attachment/([^/]+)/(feed|rdf|rss|rss2|atom)/?$\";s:49:\"index.php?attachment=$matches[1]&feed=$matches[2]\";s:93:\"index.php/[0-9]{4}/[0-9]{1,2}/[0-9]{1,2}/[^/]+/attachment/([^/]+)/comment-page-([0-9]{1,})/?$\";s:50:\"index.php?attachment=$matches[1]&cpage=$matches[2]\";s:74:\"index.php/[0-9]{4}/[0-9]{1,2}/[0-9]{1,2}/[^/]+/attachment/([^/]+)/embed/?$\";s:43:\"index.php?attachment=$matches[1]&embed=true\";s:63:\"index.php/([0-9]{4})/([0-9]{1,2})/([0-9]{1,2})/([^/]+)/embed/?$\";s:91:\"index.php?year=$matches[1]&monthnum=$matches[2]&day=$matches[3]&name=$matches[4]&embed=true\";s:67:\"index.php/([0-9]{4})/([0-9]{1,2})/([0-9]{1,2})/([^/]+)/trackback/?$\";s:85:\"index.php?year=$matches[1]&monthnum=$matches[2]&day=$matches[3]&name=$matches[4]&tb=1\";s:87:\"index.php/([0-9]{4})/([0-9]{1,2})/([0-9]{1,2})/([^/]+)/feed/(feed|rdf|rss|rss2|atom)/?$\";s:97:\"index.php?year=$matches[1]&monthnum=$matches[2]&day=$matches[3]&name=$matches[4]&feed=$matches[5]\";s:82:\"index.php/([0-9]{4})/([0-9]{1,2})/([0-9]{1,2})/([^/]+)/(feed|rdf|rss|rss2|atom)/?$\";s:97:\"index.php?year=$matches[1]&monthnum=$matches[2]&day=$matches[3]&name=$matches[4]&feed=$matches[5]\";s:75:\"index.php/([0-9]{4})/([0-9]{1,2})/([0-9]{1,2})/([^/]+)/page/?([0-9]{1,})/?$\";s:98:\"index.php?year=$matches[1]&monthnum=$matches[2]&day=$matches[3]&name=$matches[4]&paged=$matches[5]\";s:82:\"index.php/([0-9]{4})/([0-9]{1,2})/([0-9]{1,2})/([^/]+)/comment-page-([0-9]{1,})/?$\";s:98:\"index.php?year=$matches[1]&monthnum=$matches[2]&day=$matches[3]&name=$matches[4]&cpage=$matches[5]\";s:71:\"index.php/([0-9]{4})/([0-9]{1,2})/([0-9]{1,2})/([^/]+)(?:/([0-9]+))?/?$\";s:97:\"index.php?year=$matches[1]&monthnum=$matches[2]&day=$matches[3]&name=$matches[4]&page=$matches[5]\";s:57:\"index.php/[0-9]{4}/[0-9]{1,2}/[0-9]{1,2}/[^/]+/([^/]+)/?$\";s:32:\"index.php?attachment=$matches[1]\";s:67:\"index.php/[0-9]{4}/[0-9]{1,2}/[0-9]{1,2}/[^/]+/([^/]+)/trackback/?$\";s:37:\"index.php?attachment=$matches[1]&tb=1\";s:87:\"index.php/[0-9]{4}/[0-9]{1,2}/[0-9]{1,2}/[^/]+/([^/]+)/feed/(feed|rdf|rss|rss2|atom)/?$\";s:49:\"index.php?attachment=$matches[1]&feed=$matches[2]\";s:82:\"index.php/[0-9]{4}/[0-9]{1,2}/[0-9]{1,2}/[^/]+/([^/]+)/(feed|rdf|rss|rss2|atom)/?$\";s:49:\"index.php?attachment=$matches[1]&feed=$matches[2]\";s:82:\"index.php/[0-9]{4}/[0-9]{1,2}/[0-9]{1,2}/[^/]+/([^/]+)/comment-page-([0-9]{1,})/?$\";s:50:\"index.php?attachment=$matches[1]&cpage=$matches[2]\";s:63:\"index.php/[0-9]{4}/[0-9]{1,2}/[0-9]{1,2}/[^/]+/([^/]+)/embed/?$\";s:43:\"index.php?attachment=$matches[1]&embed=true\";s:74:\"index.php/([0-9]{4})/([0-9]{1,2})/([0-9]{1,2})/comment-page-([0-9]{1,})/?$\";s:81:\"index.php?year=$matches[1]&monthnum=$matches[2]&day=$matches[3]&cpage=$matches[4]\";s:61:\"index.php/([0-9]{4})/([0-9]{1,2})/comment-page-([0-9]{1,})/?$\";s:65:\"index.php?year=$matches[1]&monthnum=$matches[2]&cpage=$matches[3]\";s:48:\"index.php/([0-9]{4})/comment-page-([0-9]{1,})/?$\";s:44:\"index.php?year=$matches[1]&cpage=$matches[2]\";s:37:\"index.php/.?.+?/attachment/([^/]+)/?$\";s:32:\"index.php?attachment=$matches[1]\";s:47:\"index.php/.?.+?/attachment/([^/]+)/trackback/?$\";s:37:\"index.php?attachment=$matches[1]&tb=1\";s:67:\"index.php/.?.+?/attachment/([^/]+)/feed/(feed|rdf|rss|rss2|atom)/?$\";s:49:\"index.php?attachment=$matches[1]&feed=$matches[2]\";s:62:\"index.php/.?.+?/attachment/([^/]+)/(feed|rdf|rss|rss2|atom)/?$\";s:49:\"index.php?attachment=$matches[1]&feed=$matches[2]\";s:62:\"index.php/.?.+?/attachment/([^/]+)/comment-page-([0-9]{1,})/?$\";s:50:\"index.php?attachment=$matches[1]&cpage=$matches[2]\";s:43:\"index.php/.?.+?/attachment/([^/]+)/embed/?$\";s:43:\"index.php?attachment=$matches[1]&embed=true\";s:26:\"index.php/(.?.+?)/embed/?$\";s:41:\"index.php?pagename=$matches[1]&embed=true\";s:30:\"index.php/(.?.+?)/trackback/?$\";s:35:\"index.php?pagename=$matches[1]&tb=1\";s:50:\"index.php/(.?.+?)/feed/(feed|rdf|rss|rss2|atom)/?$\";s:47:\"index.php?pagename=$matches[1]&feed=$matches[2]\";s:45:\"index.php/(.?.+?)/(feed|rdf|rss|rss2|atom)/?$\";s:47:\"index.php?pagename=$matches[1]&feed=$matches[2]\";s:38:\"index.php/(.?.+?)/page/?([0-9]{1,})/?$\";s:48:\"index.php?pagename=$matches[1]&paged=$matches[2]\";s:45:\"index.php/(.?.+?)/comment-page-([0-9]{1,})/?$\";s:48:\"index.php?pagename=$matches[1]&cpage=$matches[2]\";s:34:\"index.php/(.?.+?)(?:/([0-9]+))?/?$\";s:47:\"index.php?pagename=$matches[1]&page=$matches[2]\";}','yes'),(30,'hack_file','0','yes'),(31,'blog_charset','UTF-8','yes'),(32,'moderation_keys','','no'),(33,'active_plugins','a:1:{i:0;s:30:\"advanced-custom-fields/acf.php\";}','yes'),(34,'category_base','','yes'),(35,'ping_sites','http://rpc.pingomatic.com/','yes'),(36,'comment_max_links','2','yes'),(37,'gmt_offset','0','yes'),(38,'default_email_category','1','yes'),(39,'recently_edited','','no'),(40,'template','twentynineteen','yes'),(41,'stylesheet','twentynineteen','yes'),(42,'comment_whitelist','1','yes'),(43,'blacklist_keys','','no'),(44,'comment_registration','0','yes'),(45,'html_type','text/html','yes'),(46,'use_trackback','0','yes'),(47,'default_role','subscriber','yes'),(48,'db_version','44719','yes'),(49,'uploads_use_yearmonth_folders','1','yes'),(50,'upload_path','','yes'),(51,'blog_public','1','yes'),(52,'default_link_category','2','yes'),(53,'show_on_front','posts','yes'),(54,'tag_base','','yes'),(55,'show_avatars','1','yes'),(56,'avatar_rating','G','yes'),(57,'upload_url_path','','yes'),(58,'thumbnail_size_w','150','yes'),(59,'thumbnail_size_h','150','yes'),(60,'thumbnail_crop','1','yes'),(61,'medium_size_w','300','yes'),(62,'medium_size_h','300','yes'),(63,'avatar_default','mystery','yes'),(64,'large_size_w','1024','yes'),(65,'large_size_h','1024','yes'),(66,'image_default_link_type','none','yes'),(67,'image_default_size','','yes'),(68,'image_default_align','','yes'),(69,'close_comments_for_old_posts','0','yes'),(70,'close_comments_days_old','14','yes'),(71,'thread_comments','1','yes'),(72,'thread_comments_depth','5','yes'),(73,'page_comments','0','yes'),(74,'comments_per_page','50','yes'),(75,'default_comments_page','newest','yes'),(76,'comment_order','asc','yes'),(77,'sticky_posts','a:0:{}','yes'),(78,'widget_categories','a:2:{i:2;a:4:{s:5:\"title\";s:0:\"\";s:5:\"count\";i:0;s:12:\"hierarchical\";i:0;s:8:\"dropdown\";i:0;}s:12:\"_multiwidget\";i:1;}','yes'),(79,'widget_text','a:0:{}','yes'),(80,'widget_rss','a:0:{}','yes'),(81,'uninstall_plugins','a:0:{}','no'),(82,'timezone_string','Europe/Warsaw','yes'),(83,'page_for_posts','0','yes'),(84,'page_on_front','0','yes'),(85,'default_post_format','0','yes'),(86,'link_manager_enabled','0','yes'),(87,'finished_splitting_shared_terms','1','yes'),(88,'site_icon','0','yes'),(89,'medium_large_size_w','768','yes'),(90,'medium_large_size_h','0','yes'),(91,'wp_page_for_privacy_policy','3','yes'),(92,'show_comments_cookies_opt_in','1','yes'),(93,'initial_db_version','44719','yes'),(94,'ra_user_roles','a:5:{s:13:\"administrator\";a:2:{s:4:\"name\";s:13:\"Administrator\";s:12:\"capabilities\";a:61:{s:13:\"switch_themes\";b:1;s:11:\"edit_themes\";b:1;s:16:\"activate_plugins\";b:1;s:12:\"edit_plugins\";b:1;s:10:\"edit_users\";b:1;s:10:\"edit_files\";b:1;s:14:\"manage_options\";b:1;s:17:\"moderate_comments\";b:1;s:17:\"manage_categories\";b:1;s:12:\"manage_links\";b:1;s:12:\"upload_files\";b:1;s:6:\"import\";b:1;s:15:\"unfiltered_html\";b:1;s:10:\"edit_posts\";b:1;s:17:\"edit_others_posts\";b:1;s:20:\"edit_published_posts\";b:1;s:13:\"publish_posts\";b:1;s:10:\"edit_pages\";b:1;s:4:\"read\";b:1;s:8:\"level_10\";b:1;s:7:\"level_9\";b:1;s:7:\"level_8\";b:1;s:7:\"level_7\";b:1;s:7:\"level_6\";b:1;s:7:\"level_5\";b:1;s:7:\"level_4\";b:1;s:7:\"level_3\";b:1;s:7:\"level_2\";b:1;s:7:\"level_1\";b:1;s:7:\"level_0\";b:1;s:17:\"edit_others_pages\";b:1;s:20:\"edit_published_pages\";b:1;s:13:\"publish_pages\";b:1;s:12:\"delete_pages\";b:1;s:19:\"delete_others_pages\";b:1;s:22:\"delete_published_pages\";b:1;s:12:\"delete_posts\";b:1;s:19:\"delete_others_posts\";b:1;s:22:\"delete_published_posts\";b:1;s:20:\"delete_private_posts\";b:1;s:18:\"edit_private_posts\";b:1;s:18:\"read_private_posts\";b:1;s:20:\"delete_private_pages\";b:1;s:18:\"edit_private_pages\";b:1;s:18:\"read_private_pages\";b:1;s:12:\"delete_users\";b:1;s:12:\"create_users\";b:1;s:17:\"unfiltered_upload\";b:1;s:14:\"edit_dashboard\";b:1;s:14:\"update_plugins\";b:1;s:14:\"delete_plugins\";b:1;s:15:\"install_plugins\";b:1;s:13:\"update_themes\";b:1;s:14:\"install_themes\";b:1;s:11:\"update_core\";b:1;s:10:\"list_users\";b:1;s:12:\"remove_users\";b:1;s:13:\"promote_users\";b:1;s:18:\"edit_theme_options\";b:1;s:13:\"delete_themes\";b:1;s:6:\"export\";b:1;}}s:6:\"editor\";a:2:{s:4:\"name\";s:6:\"Editor\";s:12:\"capabilities\";a:34:{s:17:\"moderate_comments\";b:1;s:17:\"manage_categories\";b:1;s:12:\"manage_links\";b:1;s:12:\"upload_files\";b:1;s:15:\"unfiltered_html\";b:1;s:10:\"edit_posts\";b:1;s:17:\"edit_others_posts\";b:1;s:20:\"edit_published_posts\";b:1;s:13:\"publish_posts\";b:1;s:10:\"edit_pages\";b:1;s:4:\"read\";b:1;s:7:\"level_7\";b:1;s:7:\"level_6\";b:1;s:7:\"level_5\";b:1;s:7:\"level_4\";b:1;s:7:\"level_3\";b:1;s:7:\"level_2\";b:1;s:7:\"level_1\";b:1;s:7:\"level_0\";b:1;s:17:\"edit_others_pages\";b:1;s:20:\"edit_published_pages\";b:1;s:13:\"publish_pages\";b:1;s:12:\"delete_pages\";b:1;s:19:\"delete_others_pages\";b:1;s:22:\"delete_published_pages\";b:1;s:12:\"delete_posts\";b:1;s:19:\"delete_others_posts\";b:1;s:22:\"delete_published_posts\";b:1;s:20:\"delete_private_posts\";b:1;s:18:\"edit_private_posts\";b:1;s:18:\"read_private_posts\";b:1;s:20:\"delete_private_pages\";b:1;s:18:\"edit_private_pages\";b:1;s:18:\"read_private_pages\";b:1;}}s:6:\"author\";a:2:{s:4:\"name\";s:6:\"Author\";s:12:\"capabilities\";a:10:{s:12:\"upload_files\";b:1;s:10:\"edit_posts\";b:1;s:20:\"edit_published_posts\";b:1;s:13:\"publish_posts\";b:1;s:4:\"read\";b:1;s:7:\"level_2\";b:1;s:7:\"level_1\";b:1;s:7:\"level_0\";b:1;s:12:\"delete_posts\";b:1;s:22:\"delete_published_posts\";b:1;}}s:11:\"contributor\";a:2:{s:4:\"name\";s:11:\"Contributor\";s:12:\"capabilities\";a:5:{s:10:\"edit_posts\";b:1;s:4:\"read\";b:1;s:7:\"level_1\";b:1;s:7:\"level_0\";b:1;s:12:\"delete_posts\";b:1;}}s:10:\"subscriber\";a:2:{s:4:\"name\";s:10:\"Subscriber\";s:12:\"capabilities\";a:2:{s:4:\"read\";b:1;s:7:\"level_0\";b:1;}}}','yes'),(95,'fresh_site','0','yes'),(96,'WPLANG','pl_PL','yes'),(97,'widget_search','a:2:{i:2;a:1:{s:5:\"title\";s:0:\"\";}s:12:\"_multiwidget\";i:1;}','yes'),(98,'widget_recent-posts','a:2:{i:2;a:2:{s:5:\"title\";s:0:\"\";s:6:\"number\";i:5;}s:12:\"_multiwidget\";i:1;}','yes'),(99,'widget_recent-comments','a:2:{i:2;a:2:{s:5:\"title\";s:0:\"\";s:6:\"number\";i:5;}s:12:\"_multiwidget\";i:1;}','yes'),(100,'widget_archives','a:2:{i:2;a:3:{s:5:\"title\";s:0:\"\";s:5:\"count\";i:0;s:8:\"dropdown\";i:0;}s:12:\"_multiwidget\";i:1;}','yes'),(101,'widget_meta','a:2:{i:2;a:1:{s:5:\"title\";s:0:\"\";}s:12:\"_multiwidget\";i:1;}','yes'),(102,'sidebars_widgets','a:3:{s:19:\"wp_inactive_widgets\";a:0:{}s:9:\"sidebar-1\";a:6:{i:0;s:8:\"search-2\";i:1;s:14:\"recent-posts-2\";i:2;s:17:\"recent-comments-2\";i:3;s:10:\"archives-2\";i:4;s:12:\"categories-2\";i:5;s:6:\"meta-2\";}s:13:\"array_version\";i:3;}','yes'),(103,'widget_pages','a:1:{s:12:\"_multiwidget\";i:1;}','yes'),(104,'widget_calendar','a:1:{s:12:\"_multiwidget\";i:1;}','yes'),(105,'widget_media_audio','a:1:{s:12:\"_multiwidget\";i:1;}','yes'),(106,'widget_media_image','a:1:{s:12:\"_multiwidget\";i:1;}','yes'),(107,'widget_media_gallery','a:1:{s:12:\"_multiwidget\";i:1;}','yes'),(108,'widget_media_video','a:1:{s:12:\"_multiwidget\";i:1;}','yes'),(109,'nonce_key','h>b+@x|z1YtX4CV*a`vM^(h2)&A&w/*))RZi3sDZANdKlT0Huah,JE[5!|XwQYXM','no'),(110,'nonce_salt','rt`9!;=s[fm4*3>`[OHb>Vy?)@6ZZoY[!9bk]/Elv6zBX( ,/P)[A.fD]G-UK{2{','no'),(111,'widget_tag_cloud','a:1:{s:12:\"_multiwidget\";i:1;}','yes'),(112,'widget_nav_menu','a:1:{s:12:\"_multiwidget\";i:1;}','yes'),(113,'widget_custom_html','a:1:{s:12:\"_multiwidget\";i:1;}','yes'),(114,'cron','a:5:{i:1554761896;a:1:{s:34:\"wp_privacy_delete_old_export_files\";a:1:{s:32:\"40cd750bba9870f18aada2478b24840a\";a:3:{s:8:\"schedule\";s:6:\"hourly\";s:4:\"args\";a:0:{}s:8:\"interval\";i:3600;}}}i:1554764113;a:3:{s:16:\"wp_version_check\";a:1:{s:32:\"40cd750bba9870f18aada2478b24840a\";a:3:{s:8:\"schedule\";s:10:\"twicedaily\";s:4:\"args\";a:0:{}s:8:\"interval\";i:43200;}}s:17:\"wp_update_plugins\";a:1:{s:32:\"40cd750bba9870f18aada2478b24840a\";a:3:{s:8:\"schedule\";s:10:\"twicedaily\";s:4:\"args\";a:0:{}s:8:\"interval\";i:43200;}}s:16:\"wp_update_themes\";a:1:{s:32:\"40cd750bba9870f18aada2478b24840a\";a:3:{s:8:\"schedule\";s:10:\"twicedaily\";s:4:\"args\";a:0:{}s:8:\"interval\";i:43200;}}}i:1554764125;a:2:{s:19:\"wp_scheduled_delete\";a:1:{s:32:\"40cd750bba9870f18aada2478b24840a\";a:3:{s:8:\"schedule\";s:5:\"daily\";s:4:\"args\";a:0:{}s:8:\"interval\";i:86400;}}s:25:\"delete_expired_transients\";a:1:{s:32:\"40cd750bba9870f18aada2478b24840a\";a:3:{s:8:\"schedule\";s:5:\"daily\";s:4:\"args\";a:0:{}s:8:\"interval\";i:86400;}}}i:1554764126;a:1:{s:30:\"wp_scheduled_auto_draft_delete\";a:1:{s:32:\"40cd750bba9870f18aada2478b24840a\";a:3:{s:8:\"schedule\";s:5:\"daily\";s:4:\"args\";a:0:{}s:8:\"interval\";i:86400;}}}s:7:\"version\";i:2;}','yes'),(115,'theme_mods_twentynineteen','a:1:{s:18:\"custom_css_post_id\";i:-1;}','yes'),(117,'_site_transient_update_core','O:8:\"stdClass\":4:{s:7:\"updates\";a:1:{i:0;O:8:\"stdClass\":10:{s:8:\"response\";s:6:\"latest\";s:8:\"download\";s:65:\"https://downloads.wordpress.org/release/pl_PL/wordpress-5.1.1.zip\";s:6:\"locale\";s:5:\"pl_PL\";s:8:\"packages\";O:8:\"stdClass\":5:{s:4:\"full\";s:65:\"https://downloads.wordpress.org/release/pl_PL/wordpress-5.1.1.zip\";s:10:\"no_content\";b:0;s:11:\"new_bundled\";b:0;s:7:\"partial\";b:0;s:8:\"rollback\";b:0;}s:7:\"current\";s:5:\"5.1.1\";s:7:\"version\";s:5:\"5.1.1\";s:11:\"php_version\";s:5:\"5.2.4\";s:13:\"mysql_version\";s:3:\"5.0\";s:11:\"new_bundled\";s:3:\"5.0\";s:15:\"partial_version\";s:0:\"\";}}s:12:\"last_checked\";i:1554738831;s:15:\"version_checked\";s:5:\"5.1.1\";s:12:\"translations\";a:0:{}}','no'),(122,'_site_transient_update_themes','O:8:\"stdClass\":4:{s:12:\"last_checked\";i:1554738833;s:7:\"checked\";a:3:{s:14:\"twentynineteen\";s:3:\"1.3\";s:15:\"twentyseventeen\";s:3:\"2.1\";s:13:\"twentysixteen\";s:3:\"1.9\";}s:8:\"response\";a:0:{}s:12:\"translations\";a:0:{}}','no'),(123,'auth_key','P5(;Mt<3I*c_jI5toQ|PXS<}IDKEM[d-N{TXC~7r{?m~O$WqQ1{FFP8chIy7p=9d','no'),(124,'auth_salt','RSPqPBioTF{^5AZxL(w<_(c}siZ)xz!CI#,8zLl|oL#F*YDb#adx0g5L3vS(ecGH','no'),(125,'logged_in_key','$zO,YM`1@40dbIBMsuQ8*!<z!fOON(Cd-#bEFod_AA)KwgK!p;EvbM>8f2/~eLj>','no'),(126,'logged_in_salt',',+T]/@LG5;=fCBY$UI%jx|)t8V+GxGq0r?{4(&cua]cf)^xg4A]sfRcGhLxv93L2','no'),(134,'can_compress_scripts','1','no'),(147,'recently_activated','a:0:{}','yes'),(151,'_site_transient_update_plugins','O:8:\"stdClass\":5:{s:12:\"last_checked\";i:1554738833;s:7:\"checked\";a:3:{s:30:\"advanced-custom-fields/acf.php\";s:6:\"5.7.12\";s:19:\"akismet/akismet.php\";s:5:\"4.1.1\";s:9:\"hello.php\";s:5:\"1.7.1\";}s:8:\"response\";a:0:{}s:12:\"translations\";a:0:{}s:9:\"no_update\";a:3:{s:30:\"advanced-custom-fields/acf.php\";O:8:\"stdClass\":9:{s:2:\"id\";s:36:\"w.org/plugins/advanced-custom-fields\";s:4:\"slug\";s:22:\"advanced-custom-fields\";s:6:\"plugin\";s:30:\"advanced-custom-fields/acf.php\";s:11:\"new_version\";s:6:\"5.7.12\";s:3:\"url\";s:53:\"https://wordpress.org/plugins/advanced-custom-fields/\";s:7:\"package\";s:72:\"https://downloads.wordpress.org/plugin/advanced-custom-fields.5.7.12.zip\";s:5:\"icons\";a:2:{s:2:\"2x\";s:75:\"https://ps.w.org/advanced-custom-fields/assets/icon-256x256.png?rev=1082746\";s:2:\"1x\";s:75:\"https://ps.w.org/advanced-custom-fields/assets/icon-128x128.png?rev=1082746\";}s:7:\"banners\";a:2:{s:2:\"2x\";s:78:\"https://ps.w.org/advanced-custom-fields/assets/banner-1544x500.jpg?rev=1729099\";s:2:\"1x\";s:77:\"https://ps.w.org/advanced-custom-fields/assets/banner-772x250.jpg?rev=1729102\";}s:11:\"banners_rtl\";a:0:{}}s:19:\"akismet/akismet.php\";O:8:\"stdClass\":9:{s:2:\"id\";s:21:\"w.org/plugins/akismet\";s:4:\"slug\";s:7:\"akismet\";s:6:\"plugin\";s:19:\"akismet/akismet.php\";s:11:\"new_version\";s:5:\"4.1.1\";s:3:\"url\";s:38:\"https://wordpress.org/plugins/akismet/\";s:7:\"package\";s:56:\"https://downloads.wordpress.org/plugin/akismet.4.1.1.zip\";s:5:\"icons\";a:2:{s:2:\"2x\";s:59:\"https://ps.w.org/akismet/assets/icon-256x256.png?rev=969272\";s:2:\"1x\";s:59:\"https://ps.w.org/akismet/assets/icon-128x128.png?rev=969272\";}s:7:\"banners\";a:1:{s:2:\"1x\";s:61:\"https://ps.w.org/akismet/assets/banner-772x250.jpg?rev=479904\";}s:11:\"banners_rtl\";a:0:{}}s:9:\"hello.php\";O:8:\"stdClass\":9:{s:2:\"id\";s:25:\"w.org/plugins/hello-dolly\";s:4:\"slug\";s:11:\"hello-dolly\";s:6:\"plugin\";s:9:\"hello.php\";s:11:\"new_version\";s:3:\"1.6\";s:3:\"url\";s:42:\"https://wordpress.org/plugins/hello-dolly/\";s:7:\"package\";s:58:\"https://downloads.wordpress.org/plugin/hello-dolly.1.6.zip\";s:5:\"icons\";a:2:{s:2:\"2x\";s:64:\"https://ps.w.org/hello-dolly/assets/icon-256x256.jpg?rev=2052855\";s:2:\"1x\";s:64:\"https://ps.w.org/hello-dolly/assets/icon-128x128.jpg?rev=2052855\";}s:7:\"banners\";a:1:{s:2:\"1x\";s:66:\"https://ps.w.org/hello-dolly/assets/banner-772x250.jpg?rev=2052855\";}s:11:\"banners_rtl\";a:0:{}}}}','no'),(152,'acf_version','5.7.12','yes'),(358,'_site_transient_timeout_browser_9f8f4e8c3c2a8813d95d588c455ab3d8','1554763283','no'),(359,'_site_transient_browser_9f8f4e8c3c2a8813d95d588c455ab3d8','a:10:{s:4:\"name\";s:6:\"Chrome\";s:7:\"version\";s:12:\"73.0.3683.86\";s:8:\"platform\";s:9:\"Macintosh\";s:10:\"update_url\";s:29:\"https://www.google.com/chrome\";s:7:\"img_src\";s:43:\"http://s.w.org/images/browsers/chrome.png?1\";s:11:\"img_src_ssl\";s:44:\"https://s.w.org/images/browsers/chrome.png?1\";s:15:\"current_version\";s:2:\"18\";s:7:\"upgrade\";b:0;s:8:\"insecure\";b:0;s:6:\"mobile\";b:0;}','no'),(360,'_site_transient_timeout_php_check_642e4b067f071a5f6f6d441c28391537','1554763283','no'),(361,'_site_transient_php_check_642e4b067f071a5f6f6d441c28391537','a:5:{s:19:\"recommended_version\";s:3:\"7.3\";s:15:\"minimum_version\";s:5:\"5.2.4\";s:12:\"is_supported\";b:0;s:9:\"is_secure\";b:1;s:13:\"is_acceptable\";b:1;}','no'),(449,'_site_transient_timeout_theme_roots','1554740632','no'),(450,'_site_transient_theme_roots','a:3:{s:14:\"twentynineteen\";s:7:\"/themes\";s:15:\"twentyseventeen\";s:7:\"/themes\";s:13:\"twentysixteen\";s:7:\"/themes\";}','no');
/*!40000 ALTER TABLE `ra_options` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `ra_postmeta`
--

DROP TABLE IF EXISTS `ra_postmeta`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `ra_postmeta` (
  `meta_id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `post_id` bigint(20) unsigned NOT NULL DEFAULT '0',
  `meta_key` varchar(255) COLLATE utf8mb4_unicode_520_ci DEFAULT NULL,
  `meta_value` longtext COLLATE utf8mb4_unicode_520_ci,
  PRIMARY KEY (`meta_id`),
  KEY `post_id` (`post_id`),
  KEY `meta_key` (`meta_key`(191))
) ENGINE=InnoDB AUTO_INCREMENT=163 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_520_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `ra_postmeta`
--

LOCK TABLES `ra_postmeta` WRITE;
/*!40000 ALTER TABLE `ra_postmeta` DISABLE KEYS */;
INSERT INTO `ra_postmeta` VALUES (1,2,'_wp_page_template','default'),(2,3,'_wp_page_template','default'),(3,5,'_edit_last','1'),(4,5,'_edit_lock','1553505970:1'),(5,9,'_edit_lock','1553549354:1'),(6,10,'_wp_attached_file','2019/03/photo1.png'),(7,10,'_wp_attachment_metadata','a:5:{s:5:\"width\";i:944;s:6:\"height\";i:566;s:4:\"file\";s:18:\"2019/03/photo1.png\";s:5:\"sizes\";a:3:{s:9:\"thumbnail\";a:4:{s:4:\"file\";s:18:\"photo1-150x150.png\";s:5:\"width\";i:150;s:6:\"height\";i:150;s:9:\"mime-type\";s:9:\"image/png\";}s:6:\"medium\";a:4:{s:4:\"file\";s:18:\"photo1-300x180.png\";s:5:\"width\";i:300;s:6:\"height\";i:180;s:9:\"mime-type\";s:9:\"image/png\";}s:12:\"medium_large\";a:4:{s:4:\"file\";s:18:\"photo1-768x460.png\";s:5:\"width\";i:768;s:6:\"height\";i:460;s:9:\"mime-type\";s:9:\"image/png\";}}s:10:\"image_meta\";a:12:{s:8:\"aperture\";s:1:\"0\";s:6:\"credit\";s:0:\"\";s:6:\"camera\";s:0:\"\";s:7:\"caption\";s:0:\"\";s:17:\"created_timestamp\";s:1:\"0\";s:9:\"copyright\";s:0:\"\";s:12:\"focal_length\";s:1:\"0\";s:3:\"iso\";s:1:\"0\";s:13:\"shutter_speed\";s:1:\"0\";s:5:\"title\";s:0:\"\";s:11:\"orientation\";s:1:\"0\";s:8:\"keywords\";a:0:{}}}'),(10,9,'_edit_last','1'),(13,9,'image','10'),(14,9,'_image','field_5c980c1cdc6a8'),(15,9,'author_name','KURWA'),(16,9,'_author_name','field_5c980c87dc6a9'),(17,9,'author_link','http://facebook.com'),(18,9,'_author_link','field_5c980caedc6aa'),(19,12,'image','10'),(20,12,'_image','field_5c980c1cdc6a8'),(21,12,'author_name','Pablo'),(22,12,'_author_name','field_5c980c87dc6a9'),(23,12,'author_link','http://facebook.com'),(24,12,'_author_link','field_5c980caedc6aa'),(29,13,'image','10'),(30,13,'_image','field_5c980c1cdc6a8'),(31,13,'author_name','Pablo'),(32,13,'_author_name','field_5c980c87dc6a9'),(33,13,'author_link','http://facebook.com'),(34,13,'_author_link','field_5c980caedc6aa'),(39,14,'image','10'),(40,14,'_image','field_5c980c1cdc6a8'),(41,14,'author_name','KURWA'),(42,14,'_author_name','field_5c980c87dc6a9'),(43,14,'author_link','http://facebook.com'),(44,14,'_author_link','field_5c980caedc6aa'),(49,16,'image','10'),(50,16,'_image','field_5c980c1cdc6a8'),(51,16,'author_name','KURWA'),(52,16,'_author_name','field_5c980c87dc6a9'),(53,16,'author_link','http://facebook.com'),(54,16,'_author_link','field_5c980caedc6aa'),(57,9,'_thumbnail_id','10'),(58,9,'_pingme','1'),(59,9,'_encloseme','1'),(60,17,'image','10'),(61,17,'_image','field_5c980c1cdc6a8'),(62,17,'author_name','KURWA'),(63,17,'_author_name','field_5c980c87dc6a9'),(64,17,'author_link','http://facebook.com'),(65,17,'_author_link','field_5c980caedc6aa'),(66,18,'_edit_last','1'),(67,18,'_edit_lock','1553550917:1'),(68,20,'_edit_lock','1554405992:1'),(69,20,'_edit_last','1'),(70,20,'image','22'),(71,20,'_image','field_5c994dfe3d044'),(72,22,'_wp_attached_file','2019/03/flag.png'),(73,22,'_wp_attachment_metadata','a:5:{s:5:\"width\";i:120;s:6:\"height\";i:124;s:4:\"file\";s:16:\"2019/03/flag.png\";s:5:\"sizes\";a:0:{}s:10:\"image_meta\";a:12:{s:8:\"aperture\";s:1:\"0\";s:6:\"credit\";s:0:\"\";s:6:\"camera\";s:0:\"\";s:7:\"caption\";s:0:\"\";s:17:\"created_timestamp\";s:1:\"0\";s:9:\"copyright\";s:0:\"\";s:12:\"focal_length\";s:1:\"0\";s:3:\"iso\";s:1:\"0\";s:13:\"shutter_speed\";s:1:\"0\";s:5:\"title\";s:0:\"\";s:11:\"orientation\";s:1:\"0\";s:8:\"keywords\";a:0:{}}}'),(74,23,'_edit_lock','1554189502:1'),(75,24,'_wp_attached_file','2019/03/crayon.png'),(76,24,'_wp_attachment_metadata','a:5:{s:5:\"width\";i:90;s:6:\"height\";i:131;s:4:\"file\";s:18:\"2019/03/crayon.png\";s:5:\"sizes\";a:0:{}s:10:\"image_meta\";a:12:{s:8:\"aperture\";s:1:\"0\";s:6:\"credit\";s:0:\"\";s:6:\"camera\";s:0:\"\";s:7:\"caption\";s:0:\"\";s:17:\"created_timestamp\";s:1:\"0\";s:9:\"copyright\";s:0:\"\";s:12:\"focal_length\";s:1:\"0\";s:3:\"iso\";s:1:\"0\";s:13:\"shutter_speed\";s:1:\"0\";s:5:\"title\";s:0:\"\";s:11:\"orientation\";s:1:\"0\";s:8:\"keywords\";a:0:{}}}'),(77,23,'_edit_last','1'),(78,23,'image','24'),(79,23,'_image','field_5c994dfe3d044'),(80,25,'_edit_lock','1554406002:1'),(81,26,'_wp_attached_file','2019/03/gears.png'),(82,26,'_wp_attachment_metadata','a:5:{s:5:\"width\";i:159;s:6:\"height\";i:126;s:4:\"file\";s:17:\"2019/03/gears.png\";s:5:\"sizes\";a:1:{s:9:\"thumbnail\";a:4:{s:4:\"file\";s:17:\"gears-150x126.png\";s:5:\"width\";i:150;s:6:\"height\";i:126;s:9:\"mime-type\";s:9:\"image/png\";}}s:10:\"image_meta\";a:12:{s:8:\"aperture\";s:1:\"0\";s:6:\"credit\";s:0:\"\";s:6:\"camera\";s:0:\"\";s:7:\"caption\";s:0:\"\";s:17:\"created_timestamp\";s:1:\"0\";s:9:\"copyright\";s:0:\"\";s:12:\"focal_length\";s:1:\"0\";s:3:\"iso\";s:1:\"0\";s:13:\"shutter_speed\";s:1:\"0\";s:5:\"title\";s:0:\"\";s:11:\"orientation\";s:1:\"0\";s:8:\"keywords\";a:0:{}}}'),(85,25,'_edit_last','1'),(86,25,'image','26'),(87,25,'_image','field_5c994dfe3d044'),(88,28,'_edit_lock','1554406346:1'),(89,29,'_wp_attached_file','2019/03/rocket.png'),(90,29,'_wp_attachment_metadata','a:5:{s:5:\"width\";i:95;s:6:\"height\";i:134;s:4:\"file\";s:18:\"2019/03/rocket.png\";s:5:\"sizes\";a:0:{}s:10:\"image_meta\";a:12:{s:8:\"aperture\";s:1:\"0\";s:6:\"credit\";s:0:\"\";s:6:\"camera\";s:0:\"\";s:7:\"caption\";s:0:\"\";s:17:\"created_timestamp\";s:1:\"0\";s:9:\"copyright\";s:0:\"\";s:12:\"focal_length\";s:1:\"0\";s:3:\"iso\";s:1:\"0\";s:13:\"shutter_speed\";s:1:\"0\";s:5:\"title\";s:0:\"\";s:11:\"orientation\";s:1:\"0\";s:8:\"keywords\";a:0:{}}}'),(91,28,'_edit_last','1'),(92,28,'image','29'),(93,28,'_image','field_5c994dfe3d044'),(95,31,'_edit_last','1'),(96,31,'_edit_lock','1553600844:1'),(97,33,'_edit_last','1'),(98,33,'_edit_lock','1553728950:1'),(99,35,'_edit_lock','1553602431:1'),(100,36,'_wp_attached_file','2019/03/htmlcss.png'),(101,36,'_wp_attachment_metadata','a:5:{s:5:\"width\";i:170;s:6:\"height\";i:170;s:4:\"file\";s:19:\"2019/03/htmlcss.png\";s:5:\"sizes\";a:1:{s:9:\"thumbnail\";a:4:{s:4:\"file\";s:19:\"htmlcss-150x150.png\";s:5:\"width\";i:150;s:6:\"height\";i:150;s:9:\"mime-type\";s:9:\"image/png\";}}s:10:\"image_meta\";a:12:{s:8:\"aperture\";s:1:\"0\";s:6:\"credit\";s:0:\"\";s:6:\"camera\";s:0:\"\";s:7:\"caption\";s:0:\"\";s:17:\"created_timestamp\";s:1:\"0\";s:9:\"copyright\";s:0:\"\";s:12:\"focal_length\";s:1:\"0\";s:3:\"iso\";s:1:\"0\";s:13:\"shutter_speed\";s:1:\"0\";s:5:\"title\";s:0:\"\";s:11:\"orientation\";s:1:\"0\";s:8:\"keywords\";a:0:{}}}'),(102,37,'_wp_attached_file','2019/03/jsreact.png'),(103,37,'_wp_attachment_metadata','a:5:{s:5:\"width\";i:170;s:6:\"height\";i:170;s:4:\"file\";s:19:\"2019/03/jsreact.png\";s:5:\"sizes\";a:1:{s:9:\"thumbnail\";a:4:{s:4:\"file\";s:19:\"jsreact-150x150.png\";s:5:\"width\";i:150;s:6:\"height\";i:150;s:9:\"mime-type\";s:9:\"image/png\";}}s:10:\"image_meta\";a:12:{s:8:\"aperture\";s:1:\"0\";s:6:\"credit\";s:0:\"\";s:6:\"camera\";s:0:\"\";s:7:\"caption\";s:0:\"\";s:17:\"created_timestamp\";s:1:\"0\";s:9:\"copyright\";s:0:\"\";s:12:\"focal_length\";s:1:\"0\";s:3:\"iso\";s:1:\"0\";s:13:\"shutter_speed\";s:1:\"0\";s:5:\"title\";s:0:\"\";s:11:\"orientation\";s:1:\"0\";s:8:\"keywords\";a:0:{}}}'),(104,38,'_wp_attached_file','2019/03/php.png'),(105,38,'_wp_attachment_metadata','a:5:{s:5:\"width\";i:229;s:6:\"height\";i:120;s:4:\"file\";s:15:\"2019/03/php.png\";s:5:\"sizes\";a:1:{s:9:\"thumbnail\";a:4:{s:4:\"file\";s:15:\"php-150x120.png\";s:5:\"width\";i:150;s:6:\"height\";i:120;s:9:\"mime-type\";s:9:\"image/png\";}}s:10:\"image_meta\";a:12:{s:8:\"aperture\";s:1:\"0\";s:6:\"credit\";s:0:\"\";s:6:\"camera\";s:0:\"\";s:7:\"caption\";s:0:\"\";s:17:\"created_timestamp\";s:1:\"0\";s:9:\"copyright\";s:0:\"\";s:12:\"focal_length\";s:1:\"0\";s:3:\"iso\";s:1:\"0\";s:13:\"shutter_speed\";s:1:\"0\";s:5:\"title\";s:0:\"\";s:11:\"orientation\";s:1:\"0\";s:8:\"keywords\";a:0:{}}}'),(106,39,'_wp_attached_file','2019/03/wproots.png'),(107,39,'_wp_attachment_metadata','a:5:{s:5:\"width\";i:170;s:6:\"height\";i:170;s:4:\"file\";s:19:\"2019/03/wproots.png\";s:5:\"sizes\";a:1:{s:9:\"thumbnail\";a:4:{s:4:\"file\";s:19:\"wproots-150x150.png\";s:5:\"width\";i:150;s:6:\"height\";i:150;s:9:\"mime-type\";s:9:\"image/png\";}}s:10:\"image_meta\";a:12:{s:8:\"aperture\";s:1:\"0\";s:6:\"credit\";s:0:\"\";s:6:\"camera\";s:0:\"\";s:7:\"caption\";s:0:\"\";s:17:\"created_timestamp\";s:1:\"0\";s:9:\"copyright\";s:0:\"\";s:12:\"focal_length\";s:1:\"0\";s:3:\"iso\";s:1:\"0\";s:13:\"shutter_speed\";s:1:\"0\";s:5:\"title\";s:0:\"\";s:11:\"orientation\";s:1:\"0\";s:8:\"keywords\";a:0:{}}}'),(108,35,'_edit_last','1'),(109,35,'language_logo','36'),(110,35,'_language_logo','field_5c9a11be86636'),(111,40,'_edit_lock','1553602476:1'),(112,40,'_edit_last','1'),(113,40,'language_logo','37'),(114,40,'_language_logo','field_5c9a11be86636'),(115,41,'_edit_lock','1553602492:1'),(116,41,'_edit_last','1'),(117,41,'language_logo','38'),(118,41,'_language_logo','field_5c9a11be86636'),(119,42,'_edit_lock','1553602538:1'),(120,42,'_edit_last','1'),(121,42,'language_logo','39'),(122,42,'_language_logo','field_5c9a11be86636'),(123,43,'_edit_lock','1554456825:1'),(124,44,'_wp_attached_file','2019/03/fanex.png'),(125,44,'_wp_attachment_metadata','a:5:{s:5:\"width\";i:541;s:6:\"height\";i:311;s:4:\"file\";s:17:\"2019/03/fanex.png\";s:5:\"sizes\";a:2:{s:9:\"thumbnail\";a:4:{s:4:\"file\";s:17:\"fanex-150x150.png\";s:5:\"width\";i:150;s:6:\"height\";i:150;s:9:\"mime-type\";s:9:\"image/png\";}s:6:\"medium\";a:4:{s:4:\"file\";s:17:\"fanex-300x172.png\";s:5:\"width\";i:300;s:6:\"height\";i:172;s:9:\"mime-type\";s:9:\"image/png\";}}s:10:\"image_meta\";a:12:{s:8:\"aperture\";s:1:\"0\";s:6:\"credit\";s:0:\"\";s:6:\"camera\";s:0:\"\";s:7:\"caption\";s:0:\"\";s:17:\"created_timestamp\";s:1:\"0\";s:9:\"copyright\";s:0:\"\";s:12:\"focal_length\";s:1:\"0\";s:3:\"iso\";s:1:\"0\";s:13:\"shutter_speed\";s:1:\"0\";s:5:\"title\";s:0:\"\";s:11:\"orientation\";s:1:\"0\";s:8:\"keywords\";a:0:{}}}'),(126,45,'_wp_attached_file','2019/03/goldenraven.png'),(127,45,'_wp_attachment_metadata','a:5:{s:5:\"width\";i:541;s:6:\"height\";i:311;s:4:\"file\";s:23:\"2019/03/goldenraven.png\";s:5:\"sizes\";a:2:{s:9:\"thumbnail\";a:4:{s:4:\"file\";s:23:\"goldenraven-150x150.png\";s:5:\"width\";i:150;s:6:\"height\";i:150;s:9:\"mime-type\";s:9:\"image/png\";}s:6:\"medium\";a:4:{s:4:\"file\";s:23:\"goldenraven-300x172.png\";s:5:\"width\";i:300;s:6:\"height\";i:172;s:9:\"mime-type\";s:9:\"image/png\";}}s:10:\"image_meta\";a:12:{s:8:\"aperture\";s:1:\"0\";s:6:\"credit\";s:0:\"\";s:6:\"camera\";s:0:\"\";s:7:\"caption\";s:0:\"\";s:17:\"created_timestamp\";s:1:\"0\";s:9:\"copyright\";s:0:\"\";s:12:\"focal_length\";s:1:\"0\";s:3:\"iso\";s:1:\"0\";s:13:\"shutter_speed\";s:1:\"0\";s:5:\"title\";s:0:\"\";s:11:\"orientation\";s:1:\"0\";s:8:\"keywords\";a:0:{}}}'),(128,46,'_wp_attached_file','2019/03/kisega.png'),(129,46,'_wp_attachment_metadata','a:5:{s:5:\"width\";i:541;s:6:\"height\";i:311;s:4:\"file\";s:18:\"2019/03/kisega.png\";s:5:\"sizes\";a:2:{s:9:\"thumbnail\";a:4:{s:4:\"file\";s:18:\"kisega-150x150.png\";s:5:\"width\";i:150;s:6:\"height\";i:150;s:9:\"mime-type\";s:9:\"image/png\";}s:6:\"medium\";a:4:{s:4:\"file\";s:18:\"kisega-300x172.png\";s:5:\"width\";i:300;s:6:\"height\";i:172;s:9:\"mime-type\";s:9:\"image/png\";}}s:10:\"image_meta\";a:12:{s:8:\"aperture\";s:1:\"0\";s:6:\"credit\";s:0:\"\";s:6:\"camera\";s:0:\"\";s:7:\"caption\";s:0:\"\";s:17:\"created_timestamp\";s:1:\"0\";s:9:\"copyright\";s:0:\"\";s:12:\"focal_length\";s:1:\"0\";s:3:\"iso\";s:1:\"0\";s:13:\"shutter_speed\";s:1:\"0\";s:5:\"title\";s:0:\"\";s:11:\"orientation\";s:1:\"0\";s:8:\"keywords\";a:0:{}}}'),(130,47,'_wp_attached_file','2019/03/mks.png'),(131,47,'_wp_attachment_metadata','a:5:{s:5:\"width\";i:541;s:6:\"height\";i:311;s:4:\"file\";s:15:\"2019/03/mks.png\";s:5:\"sizes\";a:2:{s:9:\"thumbnail\";a:4:{s:4:\"file\";s:15:\"mks-150x150.png\";s:5:\"width\";i:150;s:6:\"height\";i:150;s:9:\"mime-type\";s:9:\"image/png\";}s:6:\"medium\";a:4:{s:4:\"file\";s:15:\"mks-300x172.png\";s:5:\"width\";i:300;s:6:\"height\";i:172;s:9:\"mime-type\";s:9:\"image/png\";}}s:10:\"image_meta\";a:12:{s:8:\"aperture\";s:1:\"0\";s:6:\"credit\";s:0:\"\";s:6:\"camera\";s:0:\"\";s:7:\"caption\";s:0:\"\";s:17:\"created_timestamp\";s:1:\"0\";s:9:\"copyright\";s:0:\"\";s:12:\"focal_length\";s:1:\"0\";s:3:\"iso\";s:1:\"0\";s:13:\"shutter_speed\";s:1:\"0\";s:5:\"title\";s:0:\"\";s:11:\"orientation\";s:1:\"0\";s:8:\"keywords\";a:0:{}}}'),(132,43,'_edit_last','1'),(133,43,'project_image','44'),(134,43,'_project_image','field_5c9a11e7dcc62'),(135,48,'_edit_lock','1554406700:1'),(136,48,'_edit_last','1'),(137,48,'project_image','45'),(138,48,'_project_image','field_5c9a11e7dcc62'),(139,49,'_edit_lock','1554406614:1'),(140,49,'_edit_last','1'),(141,49,'project_image','47'),(142,49,'_project_image','field_5c9a11e7dcc62'),(143,50,'_edit_lock','1554409022:1'),(144,50,'_edit_last','1'),(145,50,'project_image','46'),(146,50,'_project_image','field_5c9a11e7dcc62'),(147,43,'tech_stack','HTML; Sass; Wordpress; Sage; Bedrock; JQuery; '),(148,43,'_tech_stack','field_5c9b592ee11b9'),(149,43,'link','https://fanex.pl/'),(150,43,'_link','field_5c9c06211f202'),(151,48,'tech_stack','HTML, SASS, WordPress, Sage, JQuery'),(152,48,'_tech_stack','field_5c9b592ee11b9'),(153,48,'link','https://goldenraven.pl/'),(154,48,'_link','field_5c9c06211f202'),(155,49,'tech_stack','HTML, Saas, WordPress, Sage, JQuery'),(156,49,'_tech_stack','field_5c9b592ee11b9'),(157,49,'link','http://mks.warp10.pl/'),(158,49,'_link','field_5c9c06211f202'),(159,50,'tech_stack','HTML, Sass, Jquery, WodrPress, Sage'),(160,50,'_tech_stack','field_5c9b592ee11b9'),(161,50,'link','http://lodz-biuro-rachunkowe.com.pl/'),(162,50,'_link','field_5c9c06211f202');
/*!40000 ALTER TABLE `ra_postmeta` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `ra_posts`
--

DROP TABLE IF EXISTS `ra_posts`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `ra_posts` (
  `ID` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `post_author` bigint(20) unsigned NOT NULL DEFAULT '0',
  `post_date` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `post_date_gmt` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `post_content` longtext COLLATE utf8mb4_unicode_520_ci NOT NULL,
  `post_title` text COLLATE utf8mb4_unicode_520_ci NOT NULL,
  `post_excerpt` text COLLATE utf8mb4_unicode_520_ci NOT NULL,
  `post_status` varchar(20) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT 'publish',
  `comment_status` varchar(20) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT 'open',
  `ping_status` varchar(20) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT 'open',
  `post_password` varchar(255) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `post_name` varchar(200) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `to_ping` text COLLATE utf8mb4_unicode_520_ci NOT NULL,
  `pinged` text COLLATE utf8mb4_unicode_520_ci NOT NULL,
  `post_modified` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `post_modified_gmt` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `post_content_filtered` longtext COLLATE utf8mb4_unicode_520_ci NOT NULL,
  `post_parent` bigint(20) unsigned NOT NULL DEFAULT '0',
  `guid` varchar(255) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `menu_order` int(11) NOT NULL DEFAULT '0',
  `post_type` varchar(20) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT 'post',
  `post_mime_type` varchar(100) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `comment_count` bigint(20) NOT NULL DEFAULT '0',
  PRIMARY KEY (`ID`),
  KEY `post_name` (`post_name`(191)),
  KEY `type_status_date` (`post_type`,`post_status`,`post_date`,`ID`),
  KEY `post_parent` (`post_parent`),
  KEY `post_author` (`post_author`)
) ENGINE=InnoDB AUTO_INCREMENT=57 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_520_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `ra_posts`
--

LOCK TABLES `ra_posts` WRITE;
/*!40000 ALTER TABLE `ra_posts` DISABLE KEYS */;
INSERT INTO `ra_posts` VALUES (1,1,'2019-03-24 23:55:13','2019-03-24 22:55:13','<!-- wp:paragraph -->\n<p>Witamy w WordPressie. To jest twój pierwszy post. Edytuj go lub usuń, a następnie zacznij pisać!</p>\n<!-- /wp:paragraph -->','Witaj, świecie!','','publish','open','open','','witaj-swiecie','','','2019-03-24 23:55:13','2019-03-24 22:55:13','',0,'http://localhost/reactwp/?p=1',0,'post','',1),(2,1,'2019-03-24 23:55:13','2019-03-24 22:55:13','<!-- wp:paragraph -->\n<p>To jest przykładowa strona. Strony są inne niż wpisy na blogu, ponieważ nie tylko znajdują się zawsze w jednym miejscu, ale także pojawiają się w menu witryny (w większości motywów). Większość użytkowników zaczyna od strony z informacjami o sobie, która zapozna ich przed odwiedzającymi witrynę. Taka strona może zawierać na przykład taki tekst:</p>\n<!-- /wp:paragraph -->\n\n<!-- wp:quote -->\n<blockquote class=\"wp-block-quote\"><p>Cześć! Za dnia jestem kurierem rowerowym, nocą próbuję swoich sił w aktorstwie, a to jest moja witryna. Mieszkam w Krakowie, mam wspaniałego psa który wabi się Reks i lubię piña coladę (oraz spacery, gdy pada deszcz).</p></blockquote>\n<!-- /wp:quote -->\n\n<!-- wp:paragraph -->\n<p>...albo coś takiego:</p>\n<!-- /wp:paragraph -->\n\n<!-- wp:quote -->\n<blockquote class=\"wp-block-quote\"><p>Firma XYZ Doohickey została założona w 1971 roku i od tamtej pory dostarcza społeczeństwu dobrej jakości gadżety. Znajdująca się w Gotham City XYZ zatrudnia ponad 2000 osób i robi niesamowite rzeczy dla społeczności Gotham.</p></blockquote>\n<!-- /wp:quote -->\n\n<!-- wp:paragraph -->\n<p>Jako nowy użytkownik WordPressa, powinieneś przejść do <a href=\"http://localhost/reactwp/wp-admin/\">swojego kokpitu</a> aby usunąć tę stronę i stworzyć nowe z własną treścią. Dobrej zabawy!</p>\n<!-- /wp:paragraph -->','Przykładowa strona','','publish','closed','open','','przykladowa-strona','','','2019-03-24 23:55:13','2019-03-24 22:55:13','',0,'http://localhost/reactwp/?page_id=2',0,'page','',0),(3,1,'2019-03-24 23:55:13','2019-03-24 22:55:13','<!-- wp:heading --><h2>Kim jesteśmy</h2><!-- /wp:heading --><!-- wp:paragraph --><p>Adres naszej strony internetowej to: http://localhost/reactwp.</p><!-- /wp:paragraph --><!-- wp:heading --><h2>Jakie dane osobiste zbieramy i dlaczego je zbieramy</h2><!-- /wp:heading --><!-- wp:heading {\"level\":3} --><h3>Komentarze</h3><!-- /wp:heading --><!-- wp:paragraph --><p>Kiedy odwiedzający witrynę zostawia komentarz, zbieramy dane widoczne w formularzu komentowania, jak i adres IP odwiedzającego oraz podpis jego przeglądarki jako pomoc przy wykrywaniu spamu.</p><!-- /wp:paragraph --><!-- wp:paragraph --><p>Zanonimizowany ciąg znaków stworzony na podstawie twojego adresu email (tak zwany hash) może zostać przesłany do usługi Gravatar w celu sprawdzenia czy jej używasz. Polityka prywatności usługi Gravatar jest dostępna tutaj: https://automattic.com/privacy/. Po zatwierdzeniu komentarza twój obrazek profilowy jest widoczny publicznie w kontekście twojego komentarza.</p><!-- /wp:paragraph --><!-- wp:heading {\"level\":3} --><h3>Media</h3><!-- /wp:heading --><!-- wp:paragraph --><p>Jeśli jesteś zarejestrowanym użytkownikiem i wgrywasz na witrynę obrazki, powinieneś unikać przesyłania obrazków z tagami EXIF lokalizacji. Odwiedzający stronę mogą pobrać i odczytać pełne dane lokalizacyjne z obrazków w witrynie.</p><!-- /wp:paragraph --><!-- wp:heading {\"level\":3} --><h3>Formularze kontaktowe</h3><!-- /wp:heading --><!-- wp:heading {\"level\":3} --><h3>Ciasteczka</h3><!-- /wp:heading --><!-- wp:paragraph --><p>Jeśli zostawisz na naszej witrynie komentarz, będziesz mógł wybrać opcję zapisu twojej nazwy, adresu email i adresu strony internetowej w ciasteczkach, dzięki którym podczas pisania kolejnych komentarzy powyższe informacje będą już dogodnie uzupełnione. Te ciasteczka wygasają po roku.</p><!-- /wp:paragraph --><!-- wp:paragraph --><p>Jeśli masz konto i zalogujesz się na tej witrynie, utworzymy tymczasowe ciasteczko na potrzeby sprawdzenia czy twoja przeglądarka akceptuje ciasteczka. To ciasteczko nie zawiera żadnych danych osobistych i zostanie wyrzucone kiedy zamkniesz przeglądarkę.</p><!-- /wp:paragraph --><!-- wp:paragraph --><p>Podczas logowania tworzymy dodatkowo kilka ciasteczek potrzebnych do zapisu twoich informacji logowania oraz wybranych opcji ekranu. Ciasteczka logowania wygasają po dwóch dniach, a opcji ekranu po roku. Jeśli zaznaczysz opcję &bdquo;Pamiętaj mnie&rdquo;, logowanie wygaśnie po dwóch tygodniach. Jeśli wylogujesz się ze swojego konta, ciasteczka logowania zostaną usunięte.</p><!-- /wp:paragraph --><!-- wp:paragraph --><p>Jeśli zmodyfikujesz albo opublikujesz artykuł, w twojej przeglądarce zostanie zapisane dodatkowe ciasteczko. To ciasteczko nie zawiera żadnych danych osobistych, wskazując po prostu na identyfikator przed chwilą edytowanego artykułu. Wygasa ono po 1 dniu.</p><!-- /wp:paragraph --><!-- wp:heading {\"level\":3} --><h3>Osadzone treści z innych witryn</h3><!-- /wp:heading --><!-- wp:paragraph --><p>Artykuły na tej witrynie mogą zawierać osadzone treści (np. filmy, obrazki, artykuły itp.). Osadzone treści z innych witryn zachowują się analogicznie do tego, jakby użytkownik odwiedził bezpośrednio konkretną witrynę.</p><!-- /wp:paragraph --><!-- wp:paragraph --><p>Witryny mogą zbierać informacje o tobie, używać ciasteczek, dołączać dodatkowe, zewnętrzne systemy śledzenia i monitorować twoje interakcje z osadzonym materiałem, włączając w to śledzenie twoich interakcji z osadzonym materiałem jeśli posiadasz konto i jesteś zalogowany w tamtej witrynie.</p><!-- /wp:paragraph --><!-- wp:heading {\"level\":3} --><h3>Analiza statystyk</h3><!-- /wp:heading --><!-- wp:heading --><h2>Z kim dzielimy się danymi</h2><!-- /wp:heading --><!-- wp:heading --><h2>Jak długo przechowujemy twoje dane</h2><!-- /wp:heading --><!-- wp:paragraph --><p>Jeśli zostawisz komentarz, jego treść i metadane będą przechowywane przez czas nieokreślony. Dzięki temu jesteśmy w stanie rozpoznawać i zatwierdzać kolejne komentarze automatycznie, bez wysyłania ich do każdorazowej moderacji.</p><!-- /wp:paragraph --><!-- wp:paragraph --><p>Dla użytkowników którzy zarejestrowali się na naszej stronie internetowej (jeśli tacy są), przechowujemy również informacje osobiste wprowadzone w profilu. Każdy użytkownik może dokonać wglądu, korekty albo skasować swoje informacje osobiste w dowolnej chwili (nie licząc nazwy użytkownika, której nie można zmienić). Administratorzy strony również mogą przeglądać i modyfikować te informacje.</p><!-- /wp:paragraph --><!-- wp:heading --><h2>Jakie masz prawa do swoich danych</h2><!-- /wp:heading --><!-- wp:paragraph --><p>Jeśli masz konto użytkownika albo dodałeś komentarze w tej witrynie, możesz zażądać dostarczenia pliku z wyeksportowanym kompletem twoich danych osobistych będących w naszym posiadaniu, w tym całość tych dostarczonych przez ciebie. Możesz również zażądać usunięcia przez nas całości twoich danych osobistych w naszym posiadaniu. Nie dotyczy to żadnych danych które jesteśmy zobligowani zachować ze względów administracyjnych, prawnych albo bezpieczeństwa.</p><!-- /wp:paragraph --><!-- wp:heading --><h2>Gdzie przesyłamy dane</h2><!-- /wp:heading --><!-- wp:paragraph --><p>Komentarze gości mogą być sprawdzane za pomocą automatycznej usługi wykrywania spamu.</p><!-- /wp:paragraph --><!-- wp:heading --><h2>Twoje dane kontaktowe</h2><!-- /wp:heading --><!-- wp:heading --><h2>Informacje dodatkowe</h2><!-- /wp:heading --><!-- wp:heading {\"level\":3} --><h3>Jak chronimy twoje dane?</h3><!-- /wp:heading --><!-- wp:heading {\"level\":3} --><h3>Jakie mamy obowiązujące procedury w przypadku naruszenia prywatności danych</h3><!-- /wp:heading --><!-- wp:heading {\"level\":3} --><h3>Od jakich stron trzecich otrzymujemy dane</h3><!-- /wp:heading --><!-- wp:heading {\"level\":3} --><h3>Jakie automatyczne podejmowanie decyzji i/lub tworzenie profili przeprowadzamy z użyciem danych użytkownika</h3><!-- /wp:heading --><!-- wp:heading {\"level\":3} --><h3>Branżowe wymogi regulacyjne dotyczące ujawniania informacji</h3><!-- /wp:heading -->','Polityka prywatności','','draft','closed','open','','polityka-prywatnosci','','','2019-03-24 23:55:13','2019-03-24 22:55:13','',0,'http://localhost/reactwp/?page_id=3',0,'page','',0),(5,1,'2019-03-25 00:03:24','2019-03-24 23:03:24','a:7:{s:8:\"location\";a:1:{i:0;a:1:{i:0;a:3:{s:5:\"param\";s:9:\"post_type\";s:8:\"operator\";s:2:\"==\";s:5:\"value\";s:4:\"post\";}}}s:8:\"position\";s:6:\"normal\";s:5:\"style\";s:7:\"default\";s:15:\"label_placement\";s:3:\"top\";s:21:\"instruction_placement\";s:5:\"label\";s:14:\"hide_on_screen\";s:0:\"\";s:11:\"description\";s:0:\"\";}','section','section','publish','closed','closed','','group_5c980c13a3f81','','','2019-03-25 10:28:32','2019-03-25 09:28:32','',0,'http://localhost/reactwp/?post_type=acf-field-group&#038;p=5',0,'acf-field-group','',0),(6,1,'2019-03-25 00:03:24','2019-03-24 23:03:24','a:15:{s:4:\"type\";s:5:\"image\";s:12:\"instructions\";s:0:\"\";s:8:\"required\";i:0;s:17:\"conditional_logic\";i:0;s:7:\"wrapper\";a:3:{s:5:\"width\";s:0:\"\";s:5:\"class\";s:0:\"\";s:2:\"id\";s:0:\"\";}s:13:\"return_format\";s:5:\"array\";s:12:\"preview_size\";s:9:\"thumbnail\";s:7:\"library\";s:3:\"all\";s:9:\"min_width\";s:0:\"\";s:10:\"min_height\";s:0:\"\";s:8:\"min_size\";s:0:\"\";s:9:\"max_width\";s:0:\"\";s:10:\"max_height\";s:0:\"\";s:8:\"max_size\";s:0:\"\";s:10:\"mime_types\";s:0:\"\";}','Image','image','publish','closed','closed','','field_5c980c1cdc6a8','','','2019-03-25 00:03:24','2019-03-24 23:03:24','',5,'http://localhost/reactwp/?post_type=acf-field&p=6',0,'acf-field','',0),(7,1,'2019-03-25 00:03:24','2019-03-24 23:03:24','a:10:{s:4:\"type\";s:4:\"text\";s:12:\"instructions\";s:0:\"\";s:8:\"required\";i:0;s:17:\"conditional_logic\";i:0;s:7:\"wrapper\";a:3:{s:5:\"width\";s:0:\"\";s:5:\"class\";s:0:\"\";s:2:\"id\";s:0:\"\";}s:13:\"default_value\";s:0:\"\";s:11:\"placeholder\";s:0:\"\";s:7:\"prepend\";s:0:\"\";s:6:\"append\";s:0:\"\";s:9:\"maxlength\";s:0:\"\";}','Author name','author_name','publish','closed','closed','','field_5c980c87dc6a9','','','2019-03-25 00:03:24','2019-03-24 23:03:24','',5,'http://localhost/reactwp/?post_type=acf-field&p=7',1,'acf-field','',0),(8,1,'2019-03-25 00:03:24','2019-03-24 23:03:24','a:10:{s:4:\"type\";s:4:\"text\";s:12:\"instructions\";s:0:\"\";s:8:\"required\";i:0;s:17:\"conditional_logic\";i:0;s:7:\"wrapper\";a:3:{s:5:\"width\";s:0:\"\";s:5:\"class\";s:0:\"\";s:2:\"id\";s:0:\"\";}s:13:\"default_value\";s:0:\"\";s:11:\"placeholder\";s:0:\"\";s:7:\"prepend\";s:0:\"\";s:6:\"append\";s:0:\"\";s:9:\"maxlength\";s:0:\"\";}','Author link','author_link','publish','closed','closed','','field_5c980caedc6aa','','','2019-03-25 00:03:24','2019-03-24 23:03:24','',5,'http://localhost/reactwp/?post_type=acf-field&p=8',2,'acf-field','',0),(9,1,'2019-03-25 00:04:26','2019-03-24 23:04:26','<!-- wp:paragraph -->\n<p>dasda sdasda sadas dasd asd </p>\n<!-- /wp:paragraph -->','Pablo','','publish','open','open','','9','','','2019-03-25 13:07:01','2019-03-25 12:07:01','',0,'http://localhost/reactwp/?p=9',0,'post','',0),(10,1,'2019-03-25 00:03:54','2019-03-24 23:03:54','','photo1','','inherit','open','closed','','photo1','','','2019-03-25 00:03:54','2019-03-24 23:03:54','',9,'http://localhost/reactwp/wp-content/uploads/2019/03/photo1.png',0,'attachment','image/png',0),(11,1,'2019-03-25 00:04:26','2019-03-24 23:04:26','<!-- wp:paragraph -->\n<p>Pablo</p>\n<!-- /wp:paragraph -->','','','inherit','closed','closed','','9-revision-v1','','','2019-03-25 00:04:26','2019-03-24 23:04:26','',9,'http://localhost/reactwp/index.php/2019/03/25/9-revision-v1/',0,'revision','',0),(12,1,'2019-03-25 00:04:27','2019-03-24 23:04:27','<!-- wp:paragraph -->\n<p>Pablo</p>\n<!-- /wp:paragraph -->','','','inherit','closed','closed','','9-revision-v1','','','2019-03-25 00:04:27','2019-03-24 23:04:27','',9,'http://localhost/reactwp/index.php/2019/03/25/9-revision-v1/',0,'revision','',0),(13,1,'2019-03-25 00:04:47','2019-03-24 23:04:47','','Pablo','','inherit','closed','closed','','9-revision-v1','','','2019-03-25 00:04:47','2019-03-24 23:04:47','',9,'http://localhost/reactwp/index.php/2019/03/25/9-revision-v1/',0,'revision','',0),(14,1,'2019-03-25 10:28:53','2019-03-25 09:28:53','','Pablo','','inherit','closed','closed','','9-revision-v1','','','2019-03-25 10:28:53','2019-03-25 09:28:53','',9,'http://localhost/reactwp/index.php/2019/03/25/9-revision-v1/',0,'revision','',0),(15,1,'2019-03-25 13:03:04','2019-03-25 12:03:04','<!-- wp:paragraph -->\n<p>dasda sdasda sadas dasd asd </p>\n<!-- /wp:paragraph -->','Pablo','','inherit','closed','closed','','9-revision-v1','','','2019-03-25 13:03:04','2019-03-25 12:03:04','',9,'http://localhost/reactwp/index.php/2019/03/25/9-revision-v1/',0,'revision','',0),(16,1,'2019-03-25 13:03:06','2019-03-25 12:03:06','<!-- wp:paragraph -->\n<p>dasda sdasda sadas dasd asd </p>\n<!-- /wp:paragraph -->','Pablo','','inherit','closed','closed','','9-revision-v1','','','2019-03-25 13:03:06','2019-03-25 12:03:06','',9,'http://localhost/reactwp/index.php/2019/03/25/9-revision-v1/',0,'revision','',0),(17,1,'2019-03-25 13:07:01','2019-03-25 12:07:01','<!-- wp:paragraph -->\n<p>dasda sdasda sadas dasd asd </p>\n<!-- /wp:paragraph -->','Pablo','','inherit','closed','closed','','9-revision-v1','','','2019-03-25 13:07:01','2019-03-25 12:07:01','',9,'http://localhost/reactwp/index.php/2019/03/25/9-revision-v1/',0,'revision','',0),(18,1,'2019-03-25 22:55:17','2019-03-25 21:55:17','a:7:{s:8:\"location\";a:1:{i:0;a:1:{i:0;a:3:{s:5:\"param\";s:9:\"post_type\";s:8:\"operator\";s:2:\"==\";s:5:\"value\";s:8:\"services\";}}}s:8:\"position\";s:6:\"normal\";s:5:\"style\";s:7:\"default\";s:15:\"label_placement\";s:3:\"top\";s:21:\"instruction_placement\";s:5:\"label\";s:14:\"hide_on_screen\";s:0:\"\";s:11:\"description\";s:0:\"\";}','services','services','publish','closed','closed','','group_5c994dd82ef7a','','','2019-03-25 22:55:17','2019-03-25 21:55:17','',0,'http://localhost/reactwp/?post_type=acf-field-group&#038;p=18',0,'acf-field-group','',0),(19,1,'2019-03-25 22:55:17','2019-03-25 21:55:17','a:15:{s:4:\"type\";s:5:\"image\";s:12:\"instructions\";s:0:\"\";s:8:\"required\";i:0;s:17:\"conditional_logic\";i:0;s:7:\"wrapper\";a:3:{s:5:\"width\";s:0:\"\";s:5:\"class\";s:0:\"\";s:2:\"id\";s:0:\"\";}s:13:\"return_format\";s:3:\"url\";s:12:\"preview_size\";s:9:\"thumbnail\";s:7:\"library\";s:3:\"all\";s:9:\"min_width\";s:0:\"\";s:10:\"min_height\";s:0:\"\";s:8:\"min_size\";s:0:\"\";s:9:\"max_width\";s:0:\"\";s:10:\"max_height\";s:0:\"\";s:8:\"max_size\";s:0:\"\";s:10:\"mime_types\";s:0:\"\";}','image','image','publish','closed','closed','','field_5c994dfe3d044','','','2019-03-25 22:55:17','2019-03-25 21:55:17','',18,'http://localhost/reactwp/?post_type=acf-field&p=19',0,'acf-field','',0),(20,0,'2019-03-25 22:55:45','2019-03-25 21:55:45','<!-- wp:paragraph -->\n<p> I write high quality front-end code using latest frameworks and libraries</p>\n<!-- /wp:paragraph -->','Frontend development','','publish','closed','closed','','development','','','2019-04-04 21:24:42','2019-04-04 19:24:42','',0,'http://localhost/reactwp/?post_type=services&#038;p=20',0,'services','',0),(22,1,'2019-03-25 23:30:09','2019-03-25 22:30:09','','flag','','inherit','open','closed','','flag','','','2019-03-25 23:30:09','2019-03-25 22:30:09','',20,'http://localhost/reactwp/wp-content/uploads/2019/03/flag.png',0,'attachment','image/png',0),(23,0,'2019-03-25 23:31:56','2019-03-25 22:31:56','<!-- wp:paragraph -->\n<p>I\'m creating WordPress themes from PSD projects from scrach. Pixel perfect</p>\n<!-- /wp:paragraph -->','WP Theme development','','publish','closed','closed','','theme-development','','','2019-04-02 00:43:51','2019-04-01 22:43:51','',0,'http://localhost/reactwp/?post_type=services&#038;p=23',0,'services','',0),(24,1,'2019-03-25 23:31:34','2019-03-25 22:31:34','','crayon','','inherit','open','closed','','crayon','','','2019-03-25 23:31:34','2019-03-25 22:31:34','',23,'http://localhost/reactwp/wp-content/uploads/2019/03/crayon.png',0,'attachment','image/png',0),(25,0,'2019-03-25 23:33:22','2019-03-25 22:33:22','<!-- wp:paragraph -->\n<p>Custom Content Manegment System for your app? I can provide no matter what you need.</p>\n<!-- /wp:paragraph -->','CMS Solutions','','publish','closed','closed','','cms-solutions','','','2019-04-02 00:42:33','2019-04-01 22:42:33','',0,'http://localhost/reactwp/?post_type=services&#038;p=25',0,'services','',0),(26,1,'2019-03-25 23:32:32','2019-03-25 22:32:32','','gears','','inherit','open','closed','','gears','','','2019-03-25 23:32:32','2019-03-25 22:32:32','',25,'http://localhost/reactwp/wp-content/uploads/2019/03/gears.png',0,'attachment','image/png',0),(28,0,'2019-03-25 23:33:54','2019-03-25 22:33:54','<!-- wp:paragraph -->\n<p>I can deploy your app and maintain your server to make your app online  working fast and reliably.</p>\n<!-- /wp:paragraph -->','Deployment','','publish','closed','closed','','deployment','','','2019-04-04 21:32:26','2019-04-04 19:32:26','',0,'http://localhost/reactwp/?post_type=services&#038;p=28',0,'services','',0),(29,1,'2019-03-25 23:33:50','2019-03-25 22:33:50','','rocket','','inherit','open','closed','','rocket','','','2019-03-25 23:33:50','2019-03-25 22:33:50','',28,'http://localhost/reactwp/wp-content/uploads/2019/03/rocket.png',0,'attachment','image/png',0),(31,1,'2019-03-26 12:49:46','2019-03-26 11:49:46','a:7:{s:8:\"location\";a:1:{i:0;a:1:{i:0;a:3:{s:5:\"param\";s:9:\"post_type\";s:8:\"operator\";s:2:\"==\";s:5:\"value\";s:6:\"skills\";}}}s:8:\"position\";s:6:\"normal\";s:5:\"style\";s:7:\"default\";s:15:\"label_placement\";s:3:\"top\";s:21:\"instruction_placement\";s:5:\"label\";s:14:\"hide_on_screen\";s:0:\"\";s:11:\"description\";s:0:\"\";}','skills','skills','publish','closed','closed','','group_5c9a11b3a4d2b','','','2019-03-26 12:49:46','2019-03-26 11:49:46','',0,'http://localhost/reactwp/?post_type=acf-field-group&#038;p=31',0,'acf-field-group','',0),(32,1,'2019-03-26 12:49:46','2019-03-26 11:49:46','a:15:{s:4:\"type\";s:5:\"image\";s:12:\"instructions\";s:0:\"\";s:8:\"required\";i:0;s:17:\"conditional_logic\";i:0;s:7:\"wrapper\";a:3:{s:5:\"width\";s:0:\"\";s:5:\"class\";s:0:\"\";s:2:\"id\";s:0:\"\";}s:13:\"return_format\";s:3:\"url\";s:12:\"preview_size\";s:9:\"thumbnail\";s:7:\"library\";s:3:\"all\";s:9:\"min_width\";s:0:\"\";s:10:\"min_height\";s:0:\"\";s:8:\"min_size\";s:0:\"\";s:9:\"max_width\";s:0:\"\";s:10:\"max_height\";s:0:\"\";s:8:\"max_size\";s:0:\"\";s:10:\"mime_types\";s:0:\"\";}','Language logo','language_logo','publish','closed','closed','','field_5c9a11be86636','','','2019-03-26 12:49:46','2019-03-26 11:49:46','',31,'http://localhost/reactwp/?post_type=acf-field&p=32',0,'acf-field','',0),(33,1,'2019-03-26 12:50:25','2019-03-26 11:50:25','a:7:{s:8:\"location\";a:1:{i:0;a:1:{i:0;a:3:{s:5:\"param\";s:9:\"post_type\";s:8:\"operator\";s:2:\"==\";s:5:\"value\";s:9:\"portfolio\";}}}s:8:\"position\";s:6:\"normal\";s:5:\"style\";s:7:\"default\";s:15:\"label_placement\";s:3:\"top\";s:21:\"instruction_placement\";s:5:\"label\";s:14:\"hide_on_screen\";s:0:\"\";s:11:\"description\";s:0:\"\";}','Portfolio','portfolio','publish','closed','closed','','group_5c9a11dd725a7','','','2019-03-28 00:24:52','2019-03-27 23:24:52','',0,'http://localhost/reactwp/?post_type=acf-field-group&#038;p=33',0,'acf-field-group','',0),(34,1,'2019-03-26 12:50:25','2019-03-26 11:50:25','a:15:{s:4:\"type\";s:5:\"image\";s:12:\"instructions\";s:0:\"\";s:8:\"required\";i:0;s:17:\"conditional_logic\";i:0;s:7:\"wrapper\";a:3:{s:5:\"width\";s:0:\"\";s:5:\"class\";s:0:\"\";s:2:\"id\";s:0:\"\";}s:13:\"return_format\";s:3:\"url\";s:12:\"preview_size\";s:9:\"thumbnail\";s:7:\"library\";s:3:\"all\";s:9:\"min_width\";s:0:\"\";s:10:\"min_height\";s:0:\"\";s:8:\"min_size\";s:0:\"\";s:9:\"max_width\";s:0:\"\";s:10:\"max_height\";s:0:\"\";s:8:\"max_size\";s:0:\"\";s:10:\"mime_types\";s:0:\"\";}','Project image','project_image','publish','closed','closed','','field_5c9a11e7dcc62','','','2019-03-26 12:50:25','2019-03-26 11:50:25','',33,'http://localhost/reactwp/?post_type=acf-field&p=34',0,'acf-field','',0),(35,0,'2019-03-26 13:13:51','2019-03-26 12:13:51','','HTML/CSS','','publish','closed','closed','','html-css','','','2019-03-26 13:13:51','2019-03-26 12:13:51','',0,'http://localhost/reactwp/?post_type=skills&#038;p=35',0,'skills','',0),(36,1,'2019-03-26 13:13:44','2019-03-26 12:13:44','','htmlcss','','inherit','open','closed','','htmlcss','','','2019-03-26 13:13:44','2019-03-26 12:13:44','',35,'http://localhost/reactwp/wp-content/uploads/2019/03/htmlcss.png',0,'attachment','image/png',0),(37,1,'2019-03-26 13:13:44','2019-03-26 12:13:44','','jsreact','','inherit','open','closed','','jsreact','','','2019-03-26 13:13:44','2019-03-26 12:13:44','',35,'http://localhost/reactwp/wp-content/uploads/2019/03/jsreact.png',0,'attachment','image/png',0),(38,1,'2019-03-26 13:13:45','2019-03-26 12:13:45','','php','','inherit','open','closed','','php','','','2019-03-26 13:13:45','2019-03-26 12:13:45','',35,'http://localhost/reactwp/wp-content/uploads/2019/03/php.png',0,'attachment','image/png',0),(39,1,'2019-03-26 13:13:45','2019-03-26 12:13:45','','wproots','','inherit','open','closed','','wproots','','','2019-03-26 13:13:45','2019-03-26 12:13:45','',35,'http://localhost/reactwp/wp-content/uploads/2019/03/wproots.png',0,'attachment','image/png',0),(40,0,'2019-03-26 13:14:35','2019-03-26 12:14:35','','JAVASCRIPT/REACT','','publish','closed','closed','','javascript-react','','','2019-03-26 13:14:36','2019-03-26 12:14:36','',0,'http://localhost/reactwp/?post_type=skills&#038;p=40',0,'skills','',0),(41,0,'2019-03-26 13:14:51','2019-03-26 12:14:51','','PHP','','publish','closed','closed','','php','','','2019-03-26 13:14:52','2019-03-26 12:14:52','',0,'http://localhost/reactwp/?post_type=skills&#038;p=41',0,'skills','',0),(42,0,'2019-03-26 13:15:38','2019-03-26 12:15:38','','WORDPRESS/ROOTS','','publish','closed','closed','','wordpress-roots','','','2019-03-26 13:15:38','2019-03-26 12:15:38','',0,'http://localhost/reactwp/?post_type=skills&#038;p=42',0,'skills','',0),(43,0,'2019-03-26 17:44:38','2019-03-26 16:44:38','<!-- wp:paragraph -->\n<p>Fanex company website with products, news and more </p>\n<!-- /wp:paragraph -->','Fanex','','publish','closed','closed','','fanex','','','2019-04-04 22:39:26','2019-04-04 20:39:26','',0,'http://localhost/reactwp/?post_type=portfolio&#038;p=43',0,'portfolio','',0),(44,1,'2019-03-26 17:44:03','2019-03-26 16:44:03','','fanex','','inherit','open','closed','','fanex','','','2019-03-26 17:44:03','2019-03-26 16:44:03','',43,'http://localhost/reactwp/wp-content/uploads/2019/03/fanex.png',0,'attachment','image/png',0),(45,1,'2019-03-26 17:44:04','2019-03-26 16:44:04','','goldenraven','','inherit','open','closed','','goldenraven','','','2019-03-26 17:44:04','2019-03-26 16:44:04','',43,'http://localhost/reactwp/wp-content/uploads/2019/03/goldenraven.png',0,'attachment','image/png',0),(46,1,'2019-03-26 17:44:04','2019-03-26 16:44:04','','kisega','','inherit','open','closed','','kisega','','','2019-03-26 17:44:04','2019-03-26 16:44:04','',43,'http://localhost/reactwp/wp-content/uploads/2019/03/kisega.png',0,'attachment','image/png',0),(47,1,'2019-03-26 17:44:04','2019-03-26 16:44:04','','mks','','inherit','open','closed','','mks','','','2019-03-26 17:44:04','2019-03-26 16:44:04','',43,'http://localhost/reactwp/wp-content/uploads/2019/03/mks.png',0,'attachment','image/png',0),(48,0,'2019-03-26 17:47:25','2019-03-26 16:47:25','<!-- wp:paragraph -->\n<p>Property management company website with unusual design. </p>\n<!-- /wp:paragraph -->','Golden raven','','publish','closed','closed','','golden-raven','','','2019-04-04 21:38:20','2019-04-04 19:38:20','',0,'http://localhost/reactwp/?post_type=portfolio&#038;p=48',0,'portfolio','',0),(49,0,'2019-03-26 17:47:49','2019-03-26 16:47:49','<!-- wp:paragraph -->\n<p>Swimming Club website with News, Blog, Gallery and movies from recent competitions.</p>\n<!-- /wp:paragraph -->','MKS \"Trójka\"','','publish','closed','closed','','mks-trojka','','','2019-04-04 21:36:53','2019-04-04 19:36:53','',0,'http://localhost/reactwp/?post_type=portfolio&#038;p=49',0,'portfolio','',0),(50,0,'2019-03-26 17:48:19','2019-03-26 16:48:19','<!-- wp:paragraph -->\n<p>Account office site. Simple, clear and elegant.</p>\n<!-- /wp:paragraph -->','Biuro Rachunkowe \"Księga\"','','publish','closed','closed','','biuro-rachunkowe-ksiega','','','2019-04-04 22:01:56','2019-04-04 20:01:56','',0,'http://localhost/reactwp/?post_type=portfolio&#038;p=50',0,'portfolio','',0),(52,1,'2019-03-27 12:06:47','2019-03-27 11:06:47','a:10:{s:4:\"type\";s:4:\"text\";s:12:\"instructions\";s:0:\"\";s:8:\"required\";i:0;s:17:\"conditional_logic\";i:0;s:7:\"wrapper\";a:3:{s:5:\"width\";s:0:\"\";s:5:\"class\";s:0:\"\";s:2:\"id\";s:0:\"\";}s:13:\"default_value\";s:0:\"\";s:11:\"placeholder\";s:0:\"\";s:7:\"prepend\";s:0:\"\";s:6:\"append\";s:0:\"\";s:9:\"maxlength\";s:0:\"\";}','Tech stack','tech_stack','publish','closed','closed','','field_5c9b592ee11b9','','','2019-03-27 12:06:47','2019-03-27 11:06:47','',33,'http://localhost/reactwp/?post_type=acf-field&p=52',1,'acf-field','',0),(53,1,'2019-03-28 00:24:52','2019-03-27 23:24:52','a:7:{s:4:\"type\";s:3:\"url\";s:12:\"instructions\";s:0:\"\";s:8:\"required\";i:0;s:17:\"conditional_logic\";i:0;s:7:\"wrapper\";a:3:{s:5:\"width\";s:0:\"\";s:5:\"class\";s:0:\"\";s:2:\"id\";s:0:\"\";}s:13:\"default_value\";s:0:\"\";s:11:\"placeholder\";s:0:\"\";}','Link','link','publish','closed','closed','','field_5c9c06211f202','','','2019-03-28 00:24:52','2019-03-27 23:24:52','',33,'http://localhost/reactwp/?post_type=acf-field&p=53',2,'acf-field','',0),(54,1,'2019-04-02 00:41:24','0000-00-00 00:00:00','','Automatycznie zapisany szkic','','auto-draft','open','open','','','','','2019-04-02 00:41:24','0000-00-00 00:00:00','',0,'http://localhost/reactwp/?p=54',0,'post','',0);
/*!40000 ALTER TABLE `ra_posts` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `ra_term_relationships`
--

DROP TABLE IF EXISTS `ra_term_relationships`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `ra_term_relationships` (
  `object_id` bigint(20) unsigned NOT NULL DEFAULT '0',
  `term_taxonomy_id` bigint(20) unsigned NOT NULL DEFAULT '0',
  `term_order` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`object_id`,`term_taxonomy_id`),
  KEY `term_taxonomy_id` (`term_taxonomy_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_520_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `ra_term_relationships`
--

LOCK TABLES `ra_term_relationships` WRITE;
/*!40000 ALTER TABLE `ra_term_relationships` DISABLE KEYS */;
INSERT INTO `ra_term_relationships` VALUES (1,1,0),(9,1,0);
/*!40000 ALTER TABLE `ra_term_relationships` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `ra_term_taxonomy`
--

DROP TABLE IF EXISTS `ra_term_taxonomy`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `ra_term_taxonomy` (
  `term_taxonomy_id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `term_id` bigint(20) unsigned NOT NULL DEFAULT '0',
  `taxonomy` varchar(32) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `description` longtext COLLATE utf8mb4_unicode_520_ci NOT NULL,
  `parent` bigint(20) unsigned NOT NULL DEFAULT '0',
  `count` bigint(20) NOT NULL DEFAULT '0',
  PRIMARY KEY (`term_taxonomy_id`),
  UNIQUE KEY `term_id_taxonomy` (`term_id`,`taxonomy`),
  KEY `taxonomy` (`taxonomy`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_520_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `ra_term_taxonomy`
--

LOCK TABLES `ra_term_taxonomy` WRITE;
/*!40000 ALTER TABLE `ra_term_taxonomy` DISABLE KEYS */;
INSERT INTO `ra_term_taxonomy` VALUES (1,1,'category','',0,2);
/*!40000 ALTER TABLE `ra_term_taxonomy` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `ra_termmeta`
--

DROP TABLE IF EXISTS `ra_termmeta`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `ra_termmeta` (
  `meta_id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `term_id` bigint(20) unsigned NOT NULL DEFAULT '0',
  `meta_key` varchar(255) COLLATE utf8mb4_unicode_520_ci DEFAULT NULL,
  `meta_value` longtext COLLATE utf8mb4_unicode_520_ci,
  PRIMARY KEY (`meta_id`),
  KEY `term_id` (`term_id`),
  KEY `meta_key` (`meta_key`(191))
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_520_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `ra_termmeta`
--

LOCK TABLES `ra_termmeta` WRITE;
/*!40000 ALTER TABLE `ra_termmeta` DISABLE KEYS */;
/*!40000 ALTER TABLE `ra_termmeta` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `ra_terms`
--

DROP TABLE IF EXISTS `ra_terms`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `ra_terms` (
  `term_id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(200) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `slug` varchar(200) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `term_group` bigint(10) NOT NULL DEFAULT '0',
  PRIMARY KEY (`term_id`),
  KEY `slug` (`slug`(191)),
  KEY `name` (`name`(191))
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_520_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `ra_terms`
--

LOCK TABLES `ra_terms` WRITE;
/*!40000 ALTER TABLE `ra_terms` DISABLE KEYS */;
INSERT INTO `ra_terms` VALUES (1,'Bez kategorii','bez-kategorii',0);
/*!40000 ALTER TABLE `ra_terms` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `ra_usermeta`
--

DROP TABLE IF EXISTS `ra_usermeta`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `ra_usermeta` (
  `umeta_id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `user_id` bigint(20) unsigned NOT NULL DEFAULT '0',
  `meta_key` varchar(255) COLLATE utf8mb4_unicode_520_ci DEFAULT NULL,
  `meta_value` longtext COLLATE utf8mb4_unicode_520_ci,
  PRIMARY KEY (`umeta_id`),
  KEY `user_id` (`user_id`),
  KEY `meta_key` (`meta_key`(191))
) ENGINE=InnoDB AUTO_INCREMENT=20 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_520_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `ra_usermeta`
--

LOCK TABLES `ra_usermeta` WRITE;
/*!40000 ALTER TABLE `ra_usermeta` DISABLE KEYS */;
INSERT INTO `ra_usermeta` VALUES (1,1,'nickname','admin'),(2,1,'first_name',''),(3,1,'last_name',''),(4,1,'description',''),(5,1,'rich_editing','true'),(6,1,'syntax_highlighting','true'),(7,1,'comment_shortcuts','false'),(8,1,'admin_color','fresh'),(9,1,'use_ssl','0'),(10,1,'show_admin_bar_front','true'),(11,1,'locale',''),(12,1,'ra_capabilities','a:1:{s:13:\"administrator\";b:1;}'),(13,1,'ra_user_level','10'),(14,1,'dismissed_wp_pointers','wp496_privacy'),(15,1,'show_welcome_panel','1'),(16,1,'session_tokens','a:1:{s:64:\"8e97e75dd81ae9682e4b07435ba772392604b6173f290915cf20471f96393246\";a:4:{s:10:\"expiration\";i:1554578491;s:2:\"ip\";s:3:\"::1\";s:2:\"ua\";s:120:\"Mozilla/5.0 (Macintosh; Intel Mac OS X 10_14_2) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/73.0.3683.86 Safari/537.36\";s:5:\"login\";i:1554405691;}}'),(17,1,'ra_dashboard_quick_press_last_post_id','54'),(18,1,'ra_user-settings','libraryContent=browse'),(19,1,'ra_user-settings-time','1553468663');
/*!40000 ALTER TABLE `ra_usermeta` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `ra_users`
--

DROP TABLE IF EXISTS `ra_users`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `ra_users` (
  `ID` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `user_login` varchar(60) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `user_pass` varchar(255) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `user_nicename` varchar(50) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `user_email` varchar(100) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `user_url` varchar(100) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `user_registered` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `user_activation_key` varchar(255) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `user_status` int(11) NOT NULL DEFAULT '0',
  `display_name` varchar(250) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  PRIMARY KEY (`ID`),
  KEY `user_login_key` (`user_login`),
  KEY `user_nicename` (`user_nicename`),
  KEY `user_email` (`user_email`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_520_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `ra_users`
--

LOCK TABLES `ra_users` WRITE;
/*!40000 ALTER TABLE `ra_users` DISABLE KEYS */;
INSERT INTO `ra_users` VALUES (1,'admin','$P$BJFltrK5vsIo4kOLRUHOUTke4Lb/l20','admin','pstruminski@gmail.com','','2019-03-24 22:55:13','',0,'admin');
/*!40000 ALTER TABLE `ra_users` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2019-04-08 23:19:33
