<?php

function skills_init() {
	register_post_type( 'skills', array(
		'labels'            => array(
			'name'                => __( 'Skills', 'twentynineteen' ),
			'singular_name'       => __( 'Skills', 'twentynineteen' ),
			'all_items'           => __( 'All Skills', 'twentynineteen' ),
			'new_item'            => __( 'New Skills', 'twentynineteen' ),
			'add_new'             => __( 'Add New', 'twentynineteen' ),
			'add_new_item'        => __( 'Add New Skills', 'twentynineteen' ),
			'edit_item'           => __( 'Edit Skills', 'twentynineteen' ),
			'view_item'           => __( 'View Skills', 'twentynineteen' ),
			'search_items'        => __( 'Search Skills', 'twentynineteen' ),
			'not_found'           => __( 'No Skills found', 'twentynineteen' ),
			'not_found_in_trash'  => __( 'No Skills found in trash', 'twentynineteen' ),
			'parent_item_colon'   => __( 'Parent Skills', 'twentynineteen' ),
			'menu_name'           => __( 'Skills', 'twentynineteen' ),
		),
		'public'            => true,
		'hierarchical'      => false,
		'show_ui'           => true,
		'show_in_nav_menus' => true,
		'supports'          => array( 'title', 'editor' ),
		'has_archive'       => true,
		'rewrite'           => true,
		'query_var'         => true,
		'menu_icon'         => 'dashicons-welcome-learn-more',
		'show_in_rest'      => true,
		'rest_base'         => 'skills',
		'rest_controller_class' => 'WP_REST_Posts_Controller',
	) );

}
add_action( 'init', 'skills_init' );

function skills_updated_messages( $messages ) {
	global $post;

	$permalink = get_permalink( $post );

	$messages['skills'] = array(
		0 => '', // Unused. Messages start at index 1.
		1 => sprintf( __('Skills updated. <a target="_blank" href="%s">View Skills</a>', 'twentynineteen'), esc_url( $permalink ) ),
		2 => __('Custom field updated.', 'twentynineteen'),
		3 => __('Custom field deleted.', 'twentynineteen'),
		4 => __('Skills updated.', 'twentynineteen'),
		/* translators: %s: date and time of the revision */
		5 => isset($_GET['revision']) ? sprintf( __('Skills restored to revision from %s', 'twentynineteen'), wp_post_revision_title( (int) $_GET['revision'], false ) ) : false,
		6 => sprintf( __('Skills published. <a href="%s">View Skills</a>', 'twentynineteen'), esc_url( $permalink ) ),
		7 => __('Skills saved.', 'twentynineteen'),
		8 => sprintf( __('Skills submitted. <a target="_blank" href="%s">Preview Skills</a>', 'twentynineteen'), esc_url( add_query_arg( 'preview', 'true', $permalink ) ) ),
		9 => sprintf( __('Skills scheduled for: <strong>%1$s</strong>. <a target="_blank" href="%2$s">Preview Skills</a>', 'twentynineteen'),
		// translators: Publish box date format, see https://secure.php.net/manual/en/function.date.php
		date_i18n( __( 'M j, Y @ G:i' ), strtotime( $post->post_date ) ), esc_url( $permalink ) ),
		10 => sprintf( __('Skills draft updated. <a target="_blank" href="%s">Preview Skills</a>', 'twentynineteen'), esc_url( add_query_arg( 'preview', 'true', $permalink ) ) ),
	);

	return $messages;
}
add_filter( 'post_updated_messages', 'skills_updated_messages' );
