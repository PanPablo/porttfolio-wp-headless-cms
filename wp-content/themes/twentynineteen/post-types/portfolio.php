<?php

function portfolio_init() {
	register_post_type( 'portfolio', array(
		'labels'            => array(
			'name'                => __( 'Portfolios', 'twentynineteen' ),
			'singular_name'       => __( 'Portfolio', 'twentynineteen' ),
			'all_items'           => __( 'All Portfolios', 'twentynineteen' ),
			'new_item'            => __( 'New Portfolio', 'twentynineteen' ),
			'add_new'             => __( 'Add New', 'twentynineteen' ),
			'add_new_item'        => __( 'Add New Portfolio', 'twentynineteen' ),
			'edit_item'           => __( 'Edit Portfolio', 'twentynineteen' ),
			'view_item'           => __( 'View Portfolio', 'twentynineteen' ),
			'search_items'        => __( 'Search Portfolios', 'twentynineteen' ),
			'not_found'           => __( 'No Portfolios found', 'twentynineteen' ),
			'not_found_in_trash'  => __( 'No Portfolios found in trash', 'twentynineteen' ),
			'parent_item_colon'   => __( 'Parent Portfolio', 'twentynineteen' ),
			'menu_name'           => __( 'Portfolios', 'twentynineteen' ),
		),
		'public'            => true,
		'hierarchical'      => false,
		'show_ui'           => true,
		'show_in_nav_menus' => true,
		'supports'          => array( 'title', 'editor' ),
		'has_archive'       => true,
		'rewrite'           => true,
		'query_var'         => true,
		'menu_icon'         => 'dashicons-laptop',
		'show_in_rest'      => true,
		'rest_base'         => 'portfolio',
		'rest_controller_class' => 'WP_REST_Posts_Controller',
	) );

}
add_action( 'init', 'portfolio_init' );

function portfolio_updated_messages( $messages ) {
	global $post;

	$permalink = get_permalink( $post );

	$messages['portfolio'] = array(
		0 => '', // Unused. Messages start at index 1.
		1 => sprintf( __('Portfolio updated. <a target="_blank" href="%s">View Portfolio</a>', 'twentynineteen'), esc_url( $permalink ) ),
		2 => __('Custom field updated.', 'twentynineteen'),
		3 => __('Custom field deleted.', 'twentynineteen'),
		4 => __('Portfolio updated.', 'twentynineteen'),
		/* translators: %s: date and time of the revision */
		5 => isset($_GET['revision']) ? sprintf( __('Portfolio restored to revision from %s', 'twentynineteen'), wp_post_revision_title( (int) $_GET['revision'], false ) ) : false,
		6 => sprintf( __('Portfolio published. <a href="%s">View Portfolio</a>', 'twentynineteen'), esc_url( $permalink ) ),
		7 => __('Portfolio saved.', 'twentynineteen'),
		8 => sprintf( __('Portfolio submitted. <a target="_blank" href="%s">Preview Portfolio</a>', 'twentynineteen'), esc_url( add_query_arg( 'preview', 'true', $permalink ) ) ),
		9 => sprintf( __('Portfolio scheduled for: <strong>%1$s</strong>. <a target="_blank" href="%2$s">Preview Portfolio</a>', 'twentynineteen'),
		// translators: Publish box date format, see https://secure.php.net/manual/en/function.date.php
		date_i18n( __( 'M j, Y @ G:i' ), strtotime( $post->post_date ) ), esc_url( $permalink ) ),
		10 => sprintf( __('Portfolio draft updated. <a target="_blank" href="%s">Preview Portfolio</a>', 'twentynineteen'), esc_url( add_query_arg( 'preview', 'true', $permalink ) ) ),
	);

	return $messages;
}
add_filter( 'post_updated_messages', 'portfolio_updated_messages' );
