<?php

function services_init() {
	register_post_type( 'services', array(
		'labels'            => array(
			'name'                => __( 'Services', 'twentynineteen' ),
			'singular_name'       => __( 'Services', 'twentynineteen' ),
			'all_items'           => __( 'All Services', 'twentynineteen' ),
			'new_item'            => __( 'New Services', 'twentynineteen' ),
			'add_new'             => __( 'Add New', 'twentynineteen' ),
			'add_new_item'        => __( 'Add New Services', 'twentynineteen' ),
			'edit_item'           => __( 'Edit Services', 'twentynineteen' ),
			'view_item'           => __( 'View Services', 'twentynineteen' ),
			'search_items'        => __( 'Search Services', 'twentynineteen' ),
			'not_found'           => __( 'No Services found', 'twentynineteen' ),
			'not_found_in_trash'  => __( 'No Services found in trash', 'twentynineteen' ),
			'parent_item_colon'   => __( 'Parent Services', 'twentynineteen' ),
			'menu_name'           => __( 'Services', 'twentynineteen' ),
		),
		'public'            => true,
		'hierarchical'      => false,
		'show_ui'           => true,
		'show_in_nav_menus' => true,
		'supports'          => array( 'title', 'editor' ),
		'has_archive'       => true,
		'rewrite'           => true,
		'query_var'         => true,
		'menu_icon'         => 'dashicons-analytics',
		'show_in_rest'      => true,
		'rest_base'         => 'services',
		'rest_controller_class' => 'WP_REST_Posts_Controller',
	) );

}
add_action( 'init', 'services_init' );

function services_updated_messages( $messages ) {
	global $post;

	$permalink = get_permalink( $post );

	$messages['services'] = array(
		0 => '', // Unused. Messages start at index 1.
		1 => sprintf( __('Services updated. <a target="_blank" href="%s">View Services</a>', 'twentynineteen'), esc_url( $permalink ) ),
		2 => __('Custom field updated.', 'twentynineteen'),
		3 => __('Custom field deleted.', 'twentynineteen'),
		4 => __('Services updated.', 'twentynineteen'),
		/* translators: %s: date and time of the revision */
		5 => isset($_GET['revision']) ? sprintf( __('Services restored to revision from %s', 'twentynineteen'), wp_post_revision_title( (int) $_GET['revision'], false ) ) : false,
		6 => sprintf( __('Services published. <a href="%s">View Services</a>', 'twentynineteen'), esc_url( $permalink ) ),
		7 => __('Services saved.', 'twentynineteen'),
		8 => sprintf( __('Services submitted. <a target="_blank" href="%s">Preview Services</a>', 'twentynineteen'), esc_url( add_query_arg( 'preview', 'true', $permalink ) ) ),
		9 => sprintf( __('Services scheduled for: <strong>%1$s</strong>. <a target="_blank" href="%2$s">Preview Services</a>', 'twentynineteen'),
		// translators: Publish box date format, see https://secure.php.net/manual/en/function.date.php
		date_i18n( __( 'M j, Y @ G:i' ), strtotime( $post->post_date ) ), esc_url( $permalink ) ),
		10 => sprintf( __('Services draft updated. <a target="_blank" href="%s">Preview Services</a>', 'twentynineteen'), esc_url( add_query_arg( 'preview', 'true', $permalink ) ) ),
	);

	return $messages;
}
add_filter( 'post_updated_messages', 'services_updated_messages' );
